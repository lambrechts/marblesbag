#ifndef _OBJECT_COLLECTION_H_
#define _OBJECT_COLLECTION_H_

#include <vector>
#include <string>
#include <map>

class ObjectCollection {
  std::vector<float> radius, coordorig;
  std::vector<bool> fixed;
  std::vector<float> coord, segcoord, tricoord;
  std::vector<float> circleGeom;
  std::vector<std::string> _tagname;
  int dim;
  int _step;
  std::string _filename;
  double _shift[2], _scale, _rotation[2];
  bool _clip;
  void _compute_bbox(float bbox[6]);
  std::map<int, int> _visible_flag;
  FILE *_ffmpeg;
  std::string _basename;
 public :
  ObjectCollection(const std::string basename);
  ~ObjectCollection();
  int dimension() {return dim;}
  int step() const {return _step;}
  bool file_exists(int i);

  int read(int step = -1);
  void display();

  const std::string getTagName(size_t id) const;
  void shift(double dx, double dy);
  void zoom_at(double factor, double cx, double cy);
  void rotate(double r0, double r1);

  bool clip() const {return _clip;}
  void clip(bool c) {_clip = c;}

  std::map<int, int> &visible_flag() {return _visible_flag;}

  void write_png(const char *file_name);
  void start_ffmpeg(const std::string filename);
  void stop_ffmpeg();

  void save_opt(const char *filename);
  void load_opt(const char *filename);
};
#endif
