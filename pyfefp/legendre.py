import numpy as np

class TriangleP1 :

    points = np.array([[0., 0.], [1., 0.], [0., 1.]])

    @staticmethod
    def f(pts) :
        r = np.ndarray((pts.shape[0], 3))
        r[:, 0] = 1 - pts[:, 0] - pts[:, 1]
        r[:, 1] = pts[:, 0]
        r[:, 2] = pts[:, 1]
        return r

    @staticmethod
    def df(pts) :
        d = np.ndarray((pts.shape[0], 3, 2))
        d[:, 0, 0] = -1
        d[:, 0, 1] = -1
        d[:, 1, 0] = 1
        d[:, 1, 1] = 0
        d[:, 2, 0] = 0
        d[:, 2, 1] = 1
        return d

    @staticmethod
    def dimension() :
        return 2

    @staticmethod
    def n(dim=None) :
        if dim is None :
            return 3
        elif dim == 0 :
            return 1
        else :
            return 0


class TriangleP1Mini :

    points = np.array([[0., 0.], [1., 0.], [0., 1.], [1./3, 1./3]])

    @staticmethod
    def f(pts) :
        r = np.ndarray((pts.shape[0], 4))
        r[:, 0] = 1 - pts[:, 0] - pts[:, 1]
        r[:, 1] = pts[:, 0]
        r[:, 2] = pts[:, 1]
        r[:, 3] = (1 - pts[:, 0] - pts[:, 1]) * pts[:, 0] * pts[:, 1]
        return r

    @staticmethod
    def df(pts) :
        d = np.ndarray((pts.shape[0], 4, 2))
        d[:, 0, 0] = -1
        d[:, 0, 1] = -1
        d[:, 1, 0] = 1
        d[:, 1, 1] = 0
        d[:, 2, 0] = 0
        d[:, 2, 1] = 1
        d[:, 3, 0] = ((1 - 2*pts[:, 0] - pts[:, 1]) * pts[:, 1])
        d[:, 3, 1] = ((1 - pts[:, 0] - 2*pts[:, 1]) * pts[:, 0])
        return d

    @staticmethod
    def dimension() :
        return 2

    @staticmethod
    def n(dim=None) :
        if dim is None :
            return 4
        elif dim == 0 :
            return 1
        elif dim == 2 :
            return 1
        else :
            return 0


class TetrahedronP1 :

    points = np.array([
        [0., 0., 0.],
        [1., 0., 0.],
        [0., 1., 0.],
        [0., 0., 1.]
        ])

    @staticmethod
    def f(pts) :
        r = np.ndarray((pts.shape[0], 4))
        r[:, 0] = 1 - pts[:, 0] - pts[:, 1] - pts[:, 2]
        r[:, 1] = pts[:, 0]
        r[:, 2] = pts[:, 1]
        r[:, 3] = pts[:, 2]
        return r

    @staticmethod
    def df(pts) :
        d = np.zeros((pts.shape[0], 4, 3))
        d[:, 0, 0] = -1
        d[:, 0, 1] = -1
        d[:, 0, 2] = -1
        d[:, 1, 0] = 1
        d[:, 2, 1] = 1
        d[:, 3, 2] = 1
        return d

    @staticmethod
    def dimension() :
        return 3

    @staticmethod
    def n(dim=None) :
        if dim is None :
            return 4
        elif dim == 0 :
            return 1
        else :
            return 0

class TetrahedronP1Mini :

    points = np.array([
        [0., 0., 0.],
        [1., 0., 0.],
        [0., 1., 0.],
        [0., 0., 1.],
        [0.25, 0.25, 0.25]
        ])

    @staticmethod
    def f(pts) :
        r = np.ndarray((pts.shape[0], 5))
        r[:, 0] = 1 - pts[:, 0] - pts[:, 1] - pts[:, 2]
        r[:, 1] = pts[:, 0]
        r[:, 2] = pts[:, 1]
        r[:, 3] = pts[:, 2]
        r[:, 4] = r[:, 0] * r[:, 1] * r[:, 2] * r[:, 3]
        return r

    @staticmethod
    def df(pts) :
        d = np.zeros((pts.shape[0], 5, 3))
        d[:, 0, 0] = -1
        d[:, 0, 1] = -1
        d[:, 0, 2] = -1
        d[:, 1, 0] = 1
        d[:, 2, 1] = 1
        d[:, 3, 2] = 1
        d[:, 4, 0] = pts[:, 1] * pts[:, 2] * (1 - 2*pts[:, 0] - pts[:, 1] - pts[:, 2])
        d[:, 4, 1] = pts[:, 0] * pts[:, 2] * (1 - pts[:, 0] - 2*pts[:, 1] - pts[:, 2])
        d[:, 4, 2] = pts[:, 0] * pts[:, 1] * (1 - pts[:, 0] - pts[:, 1] - 2*pts[:, 2])
        return d

    @staticmethod
    def dimension() :
        return 3

    @staticmethod
    def n(dim=None) :
        if dim is None :
            return 5
        elif dim == 0 :
            return 1
        elif dim == 3 :
            return 1
        else :
            return 0
