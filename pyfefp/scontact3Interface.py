import numpy as np
import shutil
import scontact3

class ScontactInterface :
    def __init__(self, inputfile) :
        p = scontact3.ParticleProblem()
        self._p = p
        p.load(inputfile)
        p.velocity()[:, :] = 0;
        self._r = np.mean(p.particles()[:, 0])
        self._volume = np.pi * p.particles()[:, [0]]**3 * 4./3
        self._mass = p.particles()[:, [1]]
        self._position = p.position()
        self._velocity = p.velocity()

    def volume(self):
        return self._volume

    def mass(self):
        return self._mass

    def position(self):
        return self._position

    def velocity(self):
        return self._velocity

    def setBoundaryVelocity(self, tag, v) :
        p = self._p
        tag = p.getTagId(tag)
        p.disks()[p.diskTag() == tag, 3:6] = v
        p.segments()[p.segmentTag() == tag, 6:9] = v
        p.triangles()[p.triangleTag() == tag, 9:12] = v

    def iterate(self, dt, forces) :
        self._p.externalForces()[:] = forces
        self._p.iterate(self._r, dt, 1e-5)

    def write(self, odir, i, t) :
        outputname = "%s/part-%05i" % (odir, i)
        self._p.write(outputname + "_tmp")
        shutil.move(outputname + "_tmp", outputname)

    def write_vtk(self, odir, i, t) :
        outputname = "%s/part-%05i.vtp" % (odir, i)
        self._p.writeVtk(outputname + "_tmp")
        shutil.move(outputname + "_tmp", outputname)

    def write_boundary_vtk(self, odir, i, t) :
        outputname = "%s/boundary-%05i.vtp" % (odir, i)
        self._p.writeBoundaryVtu(outputname + "_tmp")
        shutil.move(outputname + "_tmp", outputname)



