import scontact2
import numpy as np
from pyfefp import mesh
from pyfefp import scontact2Interface
import random
import os, time, sys
import shutil, random
outputdir ="output"
if not os.path.isdir(outputdir) :
    os.makedirs(outputdir)

p = scontact2.ParticleProblem()

def genInitialPosition(p, N, rmin, rmax, L, H, rhop) :
    x = np.arange(L - rmax, -L + rmax, 2 * -rmax)
    y = -np.arange(-H+rmin, H, 2* rmax);
    x, y = np.meshgrid(x, y)
    R = rmin + (rmax - rmin ) * np.random.random([N])
    x = x.flat[:N]
    y = y.flat[:N]
    for i in range(N) :
        p.addParticle((x[i], y[i]), R[i], R[i]**2 * np.pi * rhop)
    return p

r =  0.025
dr = 0.001
N = 2000
genInitialPosition(p, N = N, rmin = r -dr, rmax = r + dr , L = 2, H = 1.5, rhop=1200 * 2./3)
p.useJacobi(False)
ml = scontact2Interface.MeshLoader(p, "mesh.msh")
ml.load(["Box"])

g = 1
tEnd = 10
t = 0
iter = 0
tic = time.clock()
outputname = "%s/part-%05i" % (outputdir, 0)
p.write(outputname)
ioutput = 0
p.velocity()[:, 1] = 0;
while t < tEnd :
    p.externalForces()[:, :] = - p.velocity()
    p.externalForces()[:, 1] += -g
    p.externalForces()[:, :] *= p.particles()[:, [1]]
    dt = min(0.001, p.maxdt(r +dr)/2)
    p.iterate(r + dr, dt, 1e-5)
    t += dt
    iter += 1
    if iter %50 == 0 :
        outputname = "%s/part-%05i" % (outputdir, ioutput)
        tmpoutputname = "%s/part-%05i_tmp" % (outputdir, ioutput)
        ioutput += 1
        p.write(tmpoutputname)
        shutil.move(tmpoutputname, outputname)
print("cpu time : %g" % (time.clock() - tic))
ml.load(["Pile"], [0, 0.5])
p.write("deposited")
