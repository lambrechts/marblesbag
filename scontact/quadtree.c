#define MAXP 80
#if DIMENSION == 3
#define NSUB 8
#else
#define NSUB 4
#endif

#include "vector.h"
#include <stdio.h>
#include <math.h>
#include "quadtree.h"

typedef struct {
  int particle[MAXP];
  double xmin[MAXP][DIMENSION];
  double xmax[MAXP][DIMENSION];
} LeafData;

struct Cell_{
  double xmin[DIMENSION];
  double xmax[DIMENSION];
  LeafData *leaf;
  Cell *sub;
}; 

void *cellInit(Cell *c, const double *xmin, const double *xmax) {
  int i;
  for (i = 0; i < DIMENSION; ++i) {
    c->xmin[i] = xmin[i];
    c->xmax[i] = xmax[i];
  }
  c->sub = NULL;
  c->leaf = malloc(sizeof(LeafData));
  for (i = 0; i < MAXP; ++i)
    c->leaf->particle[i] = -1;
}

Cell *cellNew(const double *xmin, const double *xmax) {
  Cell *c = malloc(sizeof(Cell));
  cellInit(c, xmin, xmax);
  return c;
}

void cellFinalize(Cell *c) {
  if (c->leaf)
    free(c->leaf);
  int i;
  if (c->sub) {
    for (i = 0; i < NSUB; ++i)
      cellFinalize(&c->sub[i]);
    free(c->sub);
  }
}

void cellFree(Cell *c) {
  cellFinalize(c);
  free(c);
}

static void vectorInsertUnique(int **vp, int i) {
  size_t j;
  for (j = 0; j < vectorSize(*vp) && (*vp)[j] < i; ++j);
  if (j < vectorSize(*vp) && (*vp)[j] == i)
    return;
  *vectorInsert(vp, j) = i;
}

static int intersect(const double *amin, const double *amax, const double *bmin, const double *bmax) {
  if (amin[0] > bmax[0] || amax[0] < bmin[0]) return 0;
  if (amin[1] > bmax[1] || amax[1] < bmin[1]) return 0;
  #if DIMENSION == 3
  if (amin[2] > bmax[2] || amax[2] < bmin[2]) return 0;
  #endif
  return 1;
}

static void leafSearch(const LeafData *l, const double *xmin, const double *xmax, int **result) {
  for (int i = 0; i < MAXP && l->particle[i] != -1; ++i) {
    if (intersect(xmin, xmax, l->xmin[i], l->xmax[i]))
      vectorInsertUnique(result, l->particle[i]);
  }
}

void cellSearch(const Cell *c, const double *xmin, const double *xmax, int **result) {
  int i;
  if(!intersect(xmin, xmax, c->xmin, c->xmax))
    return;
  if (c->sub) {
    for (i = 0; i < NSUB; ++i)
      cellSearch(&c->sub[i], xmin, xmax, result);
  }
  else
    leafSearch(c->leaf, xmin, xmax, result);
}

void cellSplit(Cell *c) {
  int i, j;
  c->sub = malloc(NSUB * sizeof(Cell));
  #if DIMENSION ==3
  double X[DIMENSION] = {(c->xmin[0] + c->xmax[0])/2, (c->xmin[1] + c->xmax[1])/2, (c->xmin[2] + c->xmax[2]) / 2};
  for (i = 0; i < NSUB; ++i) {
    int ix = i%2;
    int iy = (i%4)/2;
    int iz = (i/4);
    double a[DIMENSION] = {ix ? X[0] : c->xmin[0], iy ? X[1] : c->xmin[1], iz ? X[2] : c->xmin[2]};
    double b[DIMENSION] = {ix ? c->xmax[0] : X[0], iy ? c->xmax[1] : X[1], iz ? c->xmax[2] : X[2]};
    cellInit(&c->sub[i], a, b);
  }
  #else
  double X[2] = {(c->xmin[0] + c->xmax[0])/2, (c->xmin[1] + c->xmax[1])/2};
  for (i = 0; i < NSUB; ++i) {
    double a[2] = {(i%2) ? X[0] : c->xmin[0], (i/2) ? X[1] : c->xmin[1]};
    double b[2] = {(i%2) ? c->xmax[0] : X[0], (i/2) ? c->xmax[1] : X[1]};
    cellInit(&c->sub[i], a, b);
  }
  #endif
  for (i = 0; i < NSUB; ++i)
    for (j = 0; j < MAXP; ++j)
      cellAdd(&c->sub[i], c->leaf->xmin[j], c->leaf->xmax[j], c->leaf->particle[j], NULL);
  free(c->leaf);
  c->leaf = NULL;
}

void cellAdd(Cell *c, const double *xmin, const double *xmax, int id, int **result) {
  int i, j;
  if (!intersect(xmin, xmax, c->xmin, c->xmax))
    return;
  if (c->leaf) {
    if (result)
      leafSearch(c->leaf, xmin, xmax, result);
    for (i = 0; i < MAXP; ++i) {
      if (c->leaf->particle[i] == -1) {
        c->leaf->particle[i] = id;
        for (j = 0; j < DIMENSION; ++j) {
          c->leaf->xmin[i][j] = xmin[j];
          c->leaf->xmax[i][j] = xmax[j];
        }
        return;
      }
    }
    cellSplit(c);
  }
  if (c->sub)
    for (i = 0; i < NSUB; ++i)
      cellAdd(&c->sub[i], xmin, xmax, id, result);
}
