\section{Particle-fluid interaction forces}

Assume a porous medium formed of particles of average diameter $d_p$ and a fluid
characterized by its dynamic viscosity $\mu$ [$\mbox{Pa} \cdot \mbox{sec}$]. 
Assume that the representative elementary volume (REV) is caracterized by a porosity $\phi$ defined as
$$
\phi = {\mbox{Volume of fluid in the REV} \over \mbox{Volume of the REV}}.
$$

Let us call $\mvu = (u_1,u_2,u_3)$ and $p$ the average fluid velocity and the average fluid pressure over a REV.
Assume the pressure gradient to be proportional both to the velocity and its Laplacian (Brinkman \cite{brinkman}, Forchheimer \cite{forchheimer}):
$$\nabla p = -{\mu \over K} \mvu -{C_E \over {\sqrt{K}}} \|\mvu\| \mvu + \bar{\mu} \nabla^2 \mvu.$$
where $K$ [$\mbox{m}^2$] is the permeability of the REV,
where $C_E$ [$\mbox{m}^{-1}$] is the Ergun coefficient that accounts for inertial (kinetic) effects
and where $\bar{\mu}$ is the effective viscosity.

The Ergun coefficient is strongly dependent on the flow regime.
For slow flows, $C_E$ is very small and the quadratic terme can be neglected.

The effective viscosity can be computed using Einstein’s formula \cite{einstein} for the effective viscosity 
of a dilute suspension of rigid spherical particles in an ambient fluid:
$$\bar{\mu} \approx \mu \left(1+{5\over 2} (1-\phi)\right).$$

The permeability is interpreted in terms of spatial parameters $\phi$ and $d_p$ 
as
$$K = {d^2_p \phi^3 \over 150 (1-\phi)^2}.$$
Let us first neglect inertial effects ($C_E = 0$). Assume a domain of charachetistic length $L \gg d_p$. The following
adimensional number (called the Fidel Castro Number $\mbox{Fc}$) accounts for the relative importance of the large scale 
viscous effects (Stokes term) and the small scale effects (Darcy term)
$$
\mbox{Fc} = {\mbox{Stokes} \over \mbox{Darcy}} = {K \over L^2} = {d^2_p \phi^3\over 150 L^2 (1-\phi)^2}.
$$ 
If a significative amount of particles are present in the REV ($\phi$ is not very close to $1$) , $\mbox{Fc} \ll 1$ and large scale effects viscous effects can be neglected. Yet, for 
$\phi \simeq 1$, stokes effects can become proeminant and this is clearly the case when no particles are present in the REV. More precisely,
if $(1-\phi) L\gg d_p$, then large scale viscous effects are negligible.
\subsection*{New}

NB : all those papers deal with gaz-particle mixture in general and fluidized bed in particular. I'm not sure this can be applied to subsurface water.
While other forces can be seen in the literature \citep{zhu_discrete_2007}, the principal forces present in the gas-particle interactions are the drag force $G$ and the pressure gradient force $F$.
\subsubsection*{Pressure gradient force}
The pressure gradient force can be easily computed by
\begin{equation}
F = -V_p∇p
\end{equation}
where $p$ is the fluid pressure field and $V_p$ the volume of the particle \citep{anderson_fluid_1967}.
\subsubsection*{Drag force}
There exists many parameterization of the drag force, \citet{li_gas-particle_2003} did a systematic study that indicates that they all possess similar predictive capability.
\citet{di_felice_fluid-particle_2012} uses the following simple formulation:
\begin{equation} \label{eq:fdp}
G = f(ε)C_d|u_p - u_f|(u_p - u_f)\frac{πd²_pρ_f}{8}
\end{equation}
where $C_d$ is the drag coefficient for an isolated particle and $f(ε)$ take into account the effect of the surrounding particles.
$C_d$ is a function of the Reynolds number of the particle $\re_p =d_pρ_f(u_p-u_f)εμ_f^{-1}$, one can use for example \citep{dallavalle_micromeritics:_1943} :
\begin{align*}
C_d &= \bigg(0.63 + \frac{4.8}{\re_p^{0.5}}\bigg)²\\
\end{align*}
The simplest expression of $f(ε)$ is \citep{wen_mechanics_1966} :
\begin{equation}
 f(ε) = ε^{-β}, \text{ with } β = 1.8.
\end{equation}
Refinements of the relationship for the parameter $β$ introduce a weak
dependency on the Reynolds number: for example, that proposed by \citet {di_felice_voidage_1994},
\begin{equation}
β = 1.8 - 0.65\exp\big(-0.5(1.5 - \log \re_p)²\big).
\end{equation}
\subsubsection*{Discretization}
\begin{enumerate}
\item $ε$ is computed as the projection of the particles volume on the mesh nodes
\item[] This is exactly like a quadrature rule where the particles position would be the quadrature points and the particle volume the corresponding weights.
\item $ε$ and $u_f$ are interpolated at the particle positions
\item $F_d$ and $F_p$ are assembled on the residual.
\begin{align*}
F_i &= ∑_pF_p(P_p, (∇P)_p)φ_i(x_p)\\
\frac{∂F_i}{∂P_j}
&=∑_p\left(\frac{∂F_p}{∂P_p}\frac{∂P_p}{∂P_j}φ_i(x_p) + \frac{∂F_p}{∂(∇P)_p}·\frac{∂(∇P)_p}{∂P_j}\right)φ_i(x_p)\\
&=∑_p\left(\frac{∂F_p}{∂P_p}φ_j(x_p) + \frac{∂F_p}{∂(∇P)_p}{·}∇φ_j(x_p)\right)φ_i(x_p)
\end{align*}
\end{enumerate}
\subsubsection*{Comparison with Darcy}
In our current implementation (Brinkman law), we have
\[
F = -ρ\frac{με²}{K₁}(u_p - u_f) + \dots\text{, with }K₁ = \frac{k₁ε³}{(1-ε)²}
\]
