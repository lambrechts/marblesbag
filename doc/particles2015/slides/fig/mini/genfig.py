from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import numpy as np
fig = plt.figure()
N = 100
X = np.arange(0, 1.0001, 0.02)
Y = np.arange(0, 1.0001, 0.02)
X, Y = np.meshgrid(X, Y)
X = X * (1-Y)
XP = X + 0.5 * Y
YP = Y * np.sqrt(0.75)
plt.axis("equal")
plt.axis("off")
plt.contourf(XP,YP,X * Y * (1-X-Y),20)
plt.savefig("P1-mini.png", bbox_inches="tight")
plt.contourf(XP,YP,X,20)
plt.savefig("P1-0.png", bbox_inches="tight")
plt.contourf(XP,YP,Y,20)
plt.savefig("P1-1.png", bbox_inches="tight")
plt.contourf(XP,YP,1-X-Y,20)
plt.savefig("P1-2.png", bbox_inches="tight")
