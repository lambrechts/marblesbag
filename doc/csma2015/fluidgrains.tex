\documentclass{beamer}
\setbeamertemplate{navigation symbols}{}
\useinnertheme{circles}

\usepackage[mathletters]{ucs}
\usepackage[utf8x]{inputenc}

\usepackage{wasysym}

\DeclareUnicodeCharacter{183}{{\,\cdot\,}}
\newcommand{\dd}[1]{\,\mbox{d}{#1}}

\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{decorations.shapes}
\usetikzlibrary{arrows, calc}
\newenvironment{proenv}{\only{\setbeamercolor{local structure}{fg=green!70!black}}}{}
\newenvironment{conenv}{\only{\setbeamercolor{local structure}{fg=red}}}{}

%thin underbrace
\makeatletter
\def\underbrace#1{%
  \@ifnextchar_{\tikz@@underbrace{#1}}{\tikz@@underbrace{#1}_{}}}
\def\tikz@@underbrace#1_#2{%
  \tikz[baseline=(a.base)] {\node[inner sep=2] (a) {\(#1\)};
  \draw[line cap=round,decorate,decoration={brace,amplitude=4pt}]
    (a.south east) -- node[pos=0.5,below,inner sep=7pt] {\(\scriptstyle #2\)} (a.south west);}}

\title[FE Fluid/Grains]{Finite Element/Discrete Element Model of Fluid/Grains Flows}
\date{CSMA - Giens 2015}

\author[J. Lambrechts]{J. Lambrechts\inst{1,2}\and F. Dubois\inst{2} \and J.-F. Remacle\inst{1}}
\institute[]{
\inst{1} Université catholique de Louvain, Belgium \\
Institute of Mechanics, Materials and Civil Engineering (IMMC)
\and 
\inst{2} Université de Montpellier 2, France \\
Laboratoire de Mécanique et Génie Civil (LMGC)
}
\newcommand{\s}{_{\text{s}}}
\renewcommand{\ss}[1]{_{\text{s}#1}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}

\begin{frame}{Fluid/Grains mixture model at any concentration}
%
\begin{block}{Applications}
\begin{itemize}
\item[\textbullet] industrial~: {\small fluidised beds, mixing, separation, pneumatic transport}
\item[\textbullet] geophysical~: {\small mudflows, landslides, submarine avalanches}
\end{itemize}
\end{block}
%
\begin{block}{Properties}
\begin{itemize}
\item[\textbullet] complex and inhomogeneous even at low concentration\\
%due to particle-particle collisions, even at low particle concentrations\\
{\small inelastic clustering, fingering, bubbling}
\item[\textbullet] hydrodynamics governed by the  particles concentration\\ and the particle-fluid and particle-particle interactions\\ %: high -> Darcy, low -> (Navier-)Stokes.
\end{itemize}
\end{block}
\end{frame}

\begin{frame}{Coupling fluid and particles}
\begin{itemize}
\item<con@3> {\color<3>{red}{DNS-DEM}}
\item Lattice-Boltzmann
\item Smooth Particles Hydrodynamics
\item <pro@2->{\color<2->{green!70!black}CFD-DEM}
\item 2 fluids
\item 1 fluid
\end{itemize}
\pause
\begin{block}{\color{green!70!black}Objective}
\begin{itemize}
\item<pro@1-> variable solid particle concentration
\item<pro@1-> inhomogeneous mixture
\item<pro@1-> structure interaction
\end{itemize}
\end{block}
\end{frame}

\begin{frame}{Coupling fluid and particles}
\framesubtitle{\normalsize 2 approaches to solve the a steady state fluid problem at each temporal iteration of the particle model.}
\vspace*{0.2cm}
\begin{columns}
\begin{column}{0.4\linewidth}
\centerline{Direct simulation}\\
\begin{tikzpicture}
\node at (0, 0) {\includegraphics[height=6cm]{fig/micro_intro.png}};
\node at (1, -1.7) {\includegraphics[height=2cm]{fig/micro_intro_zoom.png}};
\end{tikzpicture}
\end{column}
\begin{column}{0.4\linewidth}
\centerline{2-scales model}\\
\begin{tikzpicture}
\node at (0, 0) {\includegraphics[height=6cm]{fig/macro_intro.png}};
\end{tikzpicture}
\end{column}
\end{columns}
\vspace{0.2cm}
{\usebeamercolor[fg]{frametitle}Tools : }LMGC90 (particle) + GMSH (mesh generator) + FE solver\\
interacting via python/numpy.
\end{frame}

\begin{frame}{Particle model : LMGC90}
%
LMGC90 is dedicated to the numerical simulation of divided media~: granular material, masonry, fractured material, etc.
\begin{center}
\includegraphics[width=0.3\textwidth]{./fig/voie_bibloc.png}
\; 
\includegraphics[width=0.3\textwidth]{./fig/pres.jpg}
\;
\includegraphics[width=0.2\textwidth]{./fig/frag5.png}
\end{center}

It provides 
\begin{itemize}

\item a modeling framework~: rigid/FEM discretization, small or large deformations, multiple physics (mechanics, thermics, porous, etc) 

\item various simulation strategies~: Quasi-static, smooth and non-smooth Dynamics, domain decomposition method.

\end{itemize}

\end{frame}

\begin{frame}{Mesh Generator : GMSH}
GMSH is a widely used open source mesh generation software.% developed at UCLouvain and ULiège.
\begin{center}
\begin{tabular}{cc}
\includegraphics[height=3.2cm]{fig/gmsh/gbr.pdf}&
\includegraphics[height=3.2cm]{fig/gmsh/curved.pdf}\\
quad mesh & boundary layer mesh\\
\includegraphics[height=3.2cm]{fig/gmsh/curved2.pdf} &
\includegraphics[height=3.2cm]{fig/gmsh/budha1.pdf} \includegraphics[height=3cm]{fig/gmsh/budha2.pdf} \\
curvilinear mesh &
surface reparameterization
\end{tabular}
\end{center}
\end{frame}

\begin{frame}{Finite Element Solver}
\begin{itemize}
\item Spatial Discretization : (Discontinuous) Galerkin 
\item Temporal Discretization : implicit, explicit, IMEX, multirate
\item Various Physics
\end{itemize}
\begin{tabular}{cc}
\includegraphics[height=2.6cm]{fig/dg/CylinderEddies.pdf}&
\includegraphics[height=2.6cm]{fig/dg/turbofan-mr.png}\\
\includegraphics[height=3.4cm]{fig/dg/3Dbypass_vel.pdf}&
\includegraphics[height=3.4cm]{fig/dg/gbr_sol.png}
\end{tabular}
\end{frame}

\begin{frame}{Direct simulation}
\vspace*{-0.2cm}
\begin{block}<+->{Particle-resolving mesh}
\includegraphics[height=3.5cm]{fig/meshR.png}
\hspace{1cm}
\includegraphics[height=3.5cm]{fig/meshR_zoom.png}
\end{block}
\vspace*{-0.2cm}
\begin{block}<+->{Stokes Equations}
\vspace*{-1cm}
\begin{align*}
∇\cdot \mathbf u & = 0\\
∇p - μ∇²\mathbf u &= 0
\end{align*}
\end{block}
\vspace*{-0.5cm}
\begin{block}<+->{Mixed $P_1$ (pressure) - $P_1^{\text{mini}}$ (velocity) formulation}
$\underbrace{\vcenter{\hbox{
\includegraphics[width=2cm]{fig/mini/P1-2.png}
\includegraphics[width=2cm]{fig/mini/P1-0.png}
\includegraphics[width=2cm]{fig/mini/P1-1.png}}}
}_{\text{velocity and pressure}}$
$\vcenter{\hbox{\Large +}}$
$\underbrace{\vcenter{\hbox{\includegraphics[width=2cm]{fig/mini/P1-mini.png}}}}_{\text{velocity only}}$
\end{block}
\end{frame}

\begin{frame}{Direct simulation validation : comparison with Darcy's law}
\framesubtitle{
\only<1>{hexagonal arrangement}
\only<2>{square arrangement}
\only<3>{random arrangement}
}
\begin{columns}
\begin{column}{0.5\textwidth}
\includegraphics<1>[width = \textwidth]{fig/aligned_log.pdf}
\includegraphics<2>[width = \textwidth]{fig/aligned_aligned4_log.pdf}
\includegraphics<3>[width = \textwidth]{fig/aligned_aligned4_random_log.pdf}
\\
\includegraphics<1>[width = \textwidth]{fig/aligned_ratio.pdf}
\includegraphics<2>[width = \textwidth]{fig/aligned_aligned4_ratio.pdf}
\includegraphics<3>[width = \textwidth]{fig/aligned_aligned4_random_ratio.pdf}
\end{column}
\begin{column}{0.5\textwidth}

\only<1, 2>{
\includegraphics<1>[width = \textwidth]{{fig/pressure6_0.4}.png}
\includegraphics<2>[width = \textwidth]{{fig/pressure4_0.4}.png}
\\
\includegraphics<1>[width = \textwidth]{{fig/velocity6_0.4}.png}
\includegraphics<2>[width = \textwidth]{{fig/velocity4_0.4}.png}
\\
\center
$∇P=-\frac{\mathbf u}{K(ε, d)}$\\
\begin{minipage}[c][2cm]{\textwidth}
\begin{block}<only@1>{\small Kozeny-Carman}
\small $K(ε, d) = \dfrac{ε³}{150(1-ε)²}d²$
\end{block}
\begin{block}<only@2>{\small Tamayol 2009}
\tiny
$K(ε, d) = d²\bigg[\frac{12(\sqrt{φ'}-1)}{φ'\sqrt{φ'}}+\frac{18 + 12(φ'-1)}{\sqrt{φ'}(1-φ')²}$\\
          \hspace*{1.4cm}$+\frac{18\sqrt{φ'}[\text{atan}((φ'-1)^{-\frac{1}{2}}) + \frac{π}{2}]}{(φ'-1)^{\frac 52}}\bigg]^{-1} $\\
with ${φ' = \frac{π}{4(1-ε)}}$
\end{block}
\end{minipage}
}
\only<3>{
\includegraphics[width = \textwidth]{{fig/pressureR_0.4}.png}\\
\includegraphics[width = \textwidth]{{fig/velocityR_0.4}.png}
}
\vfill
\end{column}
\end{columns}
\end{frame}

\newcommand{\emean}[2]{{#1}_{\text{#2}}}
\newcommand{\imean}[2]{{#1}^{\text{#2}}}
\newcommand{\emeanb}[2]{\big({#1\big)}_{\text{#2}}}
\newcommand{\imeanb}[2]{\big({#1}\big)^{\text{#2}}}
\begin{frame}{2-scales model : Average over a fluid control volume}
\begin{columns}
\begin{column}{0.5\linewidth}
\vspace*{-0.5cm}
\begin{align*}
&&ξ &= \left\{\text{fluid : 1,} \quad \text{particle : 0}\right\}\\
\uncover<2-> {\text{porosity} &&ε &=\frac{1}{\dd{v}}\int_{\dd{v}} ξ \dd{v}_{m}\\}
\uncover<3-> {\text{volume average}&&\emean{s}{f} &=\frac{1}{\dd{v}}\int_{\dd{v}} sξ \dd{v}_{m}\\}
\uncover<3-> {\text{intrinsic average}&&\imean{s}{f} &= \frac{\emean{s}{f}}{ε}}
\end{align*}
\end{column}
\begin{column}{0.4\linewidth}
\vspace*{0.5cm}
\begin{tikzpicture}
\node[draw = red, inner sep = 0, line width=2] at(0, 0){\includegraphics[width = \linewidth]{fig/control_volume.png}};
\node at(0, -2.5) {\Large \color{red} $\dd{v}$};
\uncover<5->{
\node at(1.2, -2.8) {\Large $\dd{s}$};
\draw (1.3, -2.5) edge[out = 70, in = -30, ->, line width = 1] (1.05, -1.75);
\draw (1.3, -2.5) edge[out = 70, in = -100, ->, line width = 1] (1.45, -1.9);
}
\end{tikzpicture}
\end{column}
\end{columns}
\vspace*{-1cm}
\begin{center}
\uncover<4->{\colorbox{red!30!white}{\Large {$ s = s^f + \hat s$ }}}
\end{center}
\uncover<5-> {
\begin{align*}
\text{Gradient} && \emeanb{∇s}{f} &= ∇\emean{s}{f}+ \frac{1}{\dd{v}} \int_{\dd{s}} s \mathbf n\, \dd{s}_{m}\\
\text{Divergence} &&\emeanb{∇\mathbf u}{f} &= ∇\emean{\mathbf u}{f}+ \frac{1}{\dd{v}} \int_{\dd{s}} f \mathbf u\cdot \mathbf n\, \dd{s}_{m}
\end{align*}
}
\end{frame}

\begin{frame}{Integrated Incompressible Navier-Stokes Equations}
\ \\
\begin{block}{Volume conservation}
\[
0 = ∇·\mathbf u_s + ∇·\mathbf u_f
\]
\end{block}
\ \\
\begin{block}{Momentum conservation}
\[
\underbrace{\dfrac{∂\mathbf u_f}{∂t} + ∇·\dfrac{\mathbf u_f \mathbf u_f}{ε}}_{\text{material derivative}} = \underbrace{-∇p \vphantom{\dfrac{u_f}{ε}}}_{\text{pressure}}  + \underbrace{\dfrac{\bar {μ}}{ε}\big(∇·∇\mathbf u_f - ∇\dfrac{\mathbf u_f}{ε}·∇ε\big)}_{\text{viscous term}} + F
\]
\[F : \text{particle-fluid interaction}\]
\end{block}
\end{frame}

\begin{frame}{Parameterization of the particle-fluid forces}{}
{\usebeamercolor[fg]{frametitle}\Large F can be computed at the fluid scale}
\[F = \underbrace{-\dfrac{με²}{ρK_1(d,ε)}\mathbf u^{fs}}_{\text{Darcy}} - \underbrace{\dfrac{1}{K_2(d,ε)}|\mathbf u^{fs}|\mathbf u^{fs}}_{\text{Forccheimer}} - \dots\]\\
\vspace*{-0.5cm}
where $\mathbf u^{fs} = \dfrac{\mathbf u_f}{ε} - \dfrac{\mathbf u_s}{1-ε}$\\
\vspace*{0.5cm}
{\usebeamercolor[fg]{frametitle}\Large or at the particle scale}
\[
F = -V∇p - \underbrace{f(ε)C_d|\mathbf u^{fs}|\mathbf u^{fs}\dfrac{πd²_pρ_f}{8}}_{\text{drag}} -\dots
\]\\
\vspace*{-0.5cm}
where $C_d$ is the drag coefficient for an isolated particle\\
\vspace*{0.5cm}
{\usebeamercolor[fg]{frametitle}\Large Both models should use the same formulation.}
\end{frame}


\begin{frame}{Finite Element Formulation}
\begin{block}{Momentum Equation}
\[
\dfrac{∂\mathbf u_f}{∂t} + ∇·\dfrac{\mathbf u_f \mathbf u_f}{ε} = -∇p + \underbrace{\dfrac{\bar {μ}}{ε}\big(∇·∇\mathbf u_f - ∇\dfrac{\mathbf u_f}{ε}·∇ε\big)}_{\text{Viscous term (Stokes)}} + \underbrace{\displaystyle μK^{-1}\emean{\mathbf u}{f}\vphantom{\frac{μ}{ε}}}_{\text{Darcy}}
\]
\end{block}
\vspace*{-0.3cm}
\begin{minipage}[t][5cm]{\textwidth}
\begin{block}<only@1>{Dimensionless number}
\[
F_c =\frac{\text{Stokes}}{\text{Darcy}}= \frac{K}{L²} \approx \frac{ε³}{150(1-ε)²}\left(\frac{d}{L}\right)²
\]
The large scale viscous effects are negligible iff a significant amount of particles are present ($ε\not\approx 1$).
\end{block}
\begin{block}<only@2->{Discretization Space {\footnotesize (polynomial order $\mathcal P$)}}
\vspace*{-0.7cm}
\begin{align*}
&\text{Stokes} && ∇\imean{p}{f} ∝ ∇·∇\emean{\mathbf u}{f} &⇒&&\mathcal P_{\mathbf u} = \mathcal P_p + 1\\
&\text{Darcy} && ∇\imean{p}{f} ∝ \emean{\mathbf u}{f} &⇒&&\mathcal P_{\mathbf u} = \mathcal P_p - 1
\end{align*}
\vspace*{-0.7cm}
\end{block}
\begin{block}<only@3->{Some stabilization is required}
\begin{itemize}
\item{\color{black!30!white}additional artificial diffusivity on $\emean {\mathbf u}{f}$ and $\imean{p}{f}$ (PSPG)}
\item Mixed $P_1$ (pressure) - $P_1^{\text{mini}}$ (velocity) formulation
\end{itemize}
\end{block}
\end{minipage}
\end{frame}

\begin{frame} {Particle $⇔$ Mesh}
\framesubtitle{continuous linear lagrange shape functions with lumped mass matrix}
\begin{itemize}
\item $F_k$ defined at $N_p$ particle at position $\mathbf x_k$
\item $\hat F_j$ degrees of freedom associated to $N_φ$ shape functions $φ_j(\mathbf x)$
\end{itemize}
\[
{\color{blue}F_k} = ∑_j^{N_φ} {\color{red}\hat F_j} {\color{green!60!black}φ_j(\mathbf x_k)} \quad \quad
{\color{red}\hat F_j} = ∑_i{\color{orange!80!gray}M^{-1}_{ji}} ∑_k^{N_p} {\color{blue}F_k} {\color{green!60!black}φ_i(\mathbf x_k)}
\]
\newcommand{\rr}{3}
\begin{center}
\vspace*{-0.3cm}
\begin{tikzpicture}[scale=0.5]
\coordinate(A) at (-\rr, -1.73*\rr);
\coordinate(B) at (\rr, -1.73*\rr);
\coordinate(C) at(2*\rr, 0);
\coordinate(D) at (\rr, 1.73*\rr);
\coordinate(E) at (-\rr, 1.73*\rr);
\coordinate(F) at (-2*\rr, 0);
\coordinate(O) at(0, 0);
\clip($+1.1*(F) + 1.1*(A)!0.5!(B)$) rectangle($1.1*(C) +1.1*(D)!0.5!(E)$);
\coordinate(ABO) at ($(A)!0.5!(B)!0.3333!(O)$);
\coordinate(BCO) at ($(B)!0.5!(C)!0.3333!(O)$);
\coordinate(CDO) at ($(C)!0.5!(D)!0.3333!(O)$); 
\coordinate(DEO) at ($(D)!0.5!(E)!0.3333!(O)$); 
\coordinate(EFO) at ($(E)!0.5!(F)!0.3333!(O)$); 
\coordinate(FAO) at ($(F)!0.5!(A)!0.3333!(O)$);
\fill[orange!50!white, draw=black!20!white] (ABO) --(BCO)--(CDO) --(DEO) --(EFO) --(FAO)--cycle;
\begin{scope}[color=black!20!white]
\draw(ABO) --($(ABO) + (ABO)$);
\draw(BCO)--($(BCO) + (BCO)$) -- ($(BCO) + (BCO) +(A)!0.5!(B)$);
\draw($(BCO) + (BCO)$) --($(C) + (O)!0.5!(B)$);
\draw(CDO)--($(CDO) + (CDO)$) --($(CDO) + (CDO) + (D)!0.5!(E)$);
\draw($(CDO) + (CDO)$) --($(C) + (O)!0.5!(D)$);
\draw(DEO)--($(DEO) + (DEO)$);
\draw(EFO)--($(EFO) + (EFO)$)--($(EFO) + (EFO) + (D)!0.5!(E)$);
\draw($(EFO) + (EFO)$) --($(F) + (O)!0.5!(E)$);
\draw(FAO)--($(FAO)+ (FAO)$)--($(FAO) + (FAO) + (A)!0.5!(B)$);
\draw($(FAO) + (FAO)$) --($(F) + (O)!0.5!(A)$);
\end{scope}
 \begin{scope}[line width = 1]
\draw($-2*(A)$)--($2*(A)$);
\draw($-2*(B)$)--($2*(B)$);
\draw($-2*(C)$)--($2*(C)$);
\draw($-2*(D)$)--($2*(D)$);
\draw($-2*(E)$)--($2*(E)$);
\draw($-2*(F)$)--($2*(F)$);
\draw($-2*(C) + (B)$) --($2*(C) + (B)$);
\draw($-2*(C) + (D)$) --($2*(C) + (D)$);
\draw($(A) + (A) + (F)$) --($(D) + (D) + (F)$);
\draw($(A) + (A) + (C)$) --($(D) + (D) + (C)$);
\draw($(B) + (B) + (F)$) --($(E) + (E) + (F)$);
\draw($(B) + (B) + (C)$) --($(E) + (E) + (C)$);
\end{scope}
\draw(O) node[red]{\textbullet} node[red, below=0.3cm] {$\hat F_j$} ;
\node[blue] (P) at ($(B)!0.5!(C)!0.5!(O)$){ \textbullet};
\draw[blue] (P) node[above]{$F_k$};
\begin{scope}[green!60!black, line width=1]
\node(BN) at(B){};
\node(ON)at(O){};
\node(CN)at(C){};
\draw[<->](P) --node[below]{\small $0.5$} (ON);
\draw[<->](P) --node[right]{\small $0.25$} (BN);
\draw[<->](P) --node[below]{\small $0.25$} (CN);
 \end{scope}
\end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}{Temporal coupling}
\begin{center}
{\Large \usebeamercolor[fg]{frametitle}{Explicit coupling }}\\
(with respect to the particles)
\end{center}
\vspace*{0.2cm}
Solve the fluid $(\mathbf u_f^n, p^n)$ using $(\mathbf u_s^n, ε^n)$\\
\[
\left\{
\begin{aligned}
0 &= ∇·(\mathbf u^n_f + \mathbf u^n\s)\\
0 &= ∇p^n + D(\mathbf u^n, ε^n) + F(\mathbf u^n_f, \mathbf u^n_s, ε^n, p^n)
\end{aligned} \right.
\]\\
\ \\
Then solve the particles\\
\[
\mathbf u_s^{n+1}, ε^{n+1} = C(\mathbf u_s^{n}, ε^{n}, F(\mathbf u^n_f, \mathbf u^n_s, ε^n, p^n))
\]
\end{frame}

\begin{frame}{Preliminary result : falling drop}
\begin{columns}
\begin{column}{0.7\linewidth}
\href{run:./anim/drop.mp4}{\includegraphics[height=0.8\textheight]{fig/drop.png}}
\end{column}
\begin{column}{0.35\linewidth}
\begin{itemize}
\item {4905 particles}
\item {2000 time steps}
\item {1 hour}
\end{itemize}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Preliminary result : falling drop}
\hspace*{-0.6cm}
\includegraphics[width=1.1\textwidth]{fig/drop-field.png}
\end{frame}



\begin{frame}{Preliminary result : falling small particles}
\begin{columns}
\begin{column}{0.7\linewidth}
\href{run:./anim/depot2.mp4}{\includegraphics[height=0.8\textheight]{fig/depot2.png}}
\end{column}
\begin{column}{0.35\linewidth}
\begin{itemize}
\item {20000 particles}
\item {20000 time steps}
\item {15 hours}
\end{itemize}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Preliminary result : falling small particles}
\hspace*{-0.6cm}
\includegraphics[width=1.1\textwidth]{fig/depot-field.png}
\end{frame}
\begin{frame}{Temporal coupling}
\begin{center}
{\Large \usebeamercolor[fg]{frametitle}{Explicit coupling }}\\
(with respect to the particles)
\end{center}
\vspace*{0.2cm}
Solve the fluid $(\mathbf u_f^n, p^n)$ using $(\mathbf u_s^n, ε^n)$\\
\[
\left\{
\begin{aligned}
0 &= ∇·(\mathbf u^n_f + \mathbf u^n\s)\\
0 &= ∇p^n + D(\mathbf u^n, ε^n) + F(\mathbf u^n_f, \mathbf u^n_s, ε^n, p^n)
\end{aligned} \right.
\]\\
\ \\
Then solve the particles\\
\[
\mathbf u_s^{n+1}, ε^{n+1} = C(\mathbf u_s^{n}, ε^{n}, F(\mathbf u^n_f, \mathbf u^n_s, ε^n, p^n))
\]
\vspace*{0.1cm}
\begin{center}
{\Large \alert{\[Δt \propto \frac{r_p}{v_p} \hspace{1cm} Δt \propto \frac{r_p^2}{μ}\]}}
\end{center}
\end{frame}

\begin{frame}{Temporal coupling}
\begin{center}
{\Large \usebeamercolor[fg]{frametitle}{Enhanced explicit  coupling }}
\end{center}
\vspace*{0.2cm}
Solve the fluid $(\mathbf u_f^n, p^n)$ using $(\mathbf u_s^n, ε^n)$ and an approx. of $\mathbf u_\text{free}^{n+1}$\\
\[
\left\{
\begin{aligned}
0 &= ∇·(\mathbf u^n_f + \mathbf u^n\s + \frac{Δt}{ρ_s}ε^nF(\mathbf u^n_f, \mathbf u^n_s, ε^n, p^n))\\
0 &= ∇p^n + D(\mathbf u^n, ε^n) + F(\mathbf u^n_f, \mathbf u^n_s, ε^n, p^n)
\end{aligned} \right.
\]\\
{\small \flushright where $\mathbf u_{\text free}^{n+1}≃\mathbf u^n\s + \frac{Δt}{ρ_s}ε^nF(\mathbf u^n_f, \mathbf u^n_s, ε^n, p^n)$.\\}
Then solve the particles\\
\[
\mathbf u_s^{n+1}, ε^{n+1} = C(\mathbf u_s^{n}, ε^{n}, F(\mathbf u^n_f, \mathbf u^n_s, ε^n, p^n))
\]
\begin{center}
\ \\
{\Large \alert{Stable but inconsistant}}\\because $\mathbf u_{\text free}^{n+1}$ is not an approximation of $\mathbf u_s^{n+1}$
\end{center}
\end{frame}

\begin{frame}{Preliminary result : particles in a circular Couette flow}
\begin{columns}
\begin{column}{0.7\linewidth}
\href{run:./anim/couette2dmu05.mp4}{\includegraphics[height=0.8\textheight]{fig/couette.png}}
\end{column}
\begin{column}{0.35\linewidth}
\begin{itemize}
\item {4140 particles}
\item {few contacts}
\item {10000 time steps}
\item {3 hours\\or 10 minutes}
\end{itemize}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Temporal coupling}
\begin{center}
{\Large \usebeamercolor[fg]{frametitle}{Implicit  coupling }}\\
\end{center}
\vspace*{0.2cm}
Solve everything as a single non-linear system\\
\[
\left\{
\begin{aligned}
\mathbf u_s^{n+1}, ε^{n+1} &= C(\mathbf u_s^{n}, ε^{n}, F(\mathbf u^n_f, \mathbf u^n_s, ε^n, p^n))\\
0 &= ∇·(\mathbf u^n_f + \mathbf u^{n+1}\s)\\
0 &= ∇p^n + D(\mathbf u^n, ε^n) + F(\mathbf u^n_f, \mathbf u^n_s, ε^n, p^n)
\end{aligned} \right.
\]\\
\ \\
\vspace*{0.1cm}
\begin{center}
{\Large Stable and consistant but \\\vspace*{0.1cm}\alert{(a continuous approximation of) $\frac{∂\mathbf u^{n+1}_s}{∂(\mathbf u_f^n, p^n)}$ is required.}}
\end{center}
\end{frame}


\begin{frame}{Preliminary result : Pile}
\begin{columns}
\begin{column}{0.7\linewidth}
\href{run:./anim/pile3dcolor.mp4}{\includegraphics[height=0.8\textheight]{fig/pile3dcolor.png}}
\end{column}
\begin{column}{0.35\linewidth}
\begin{itemize}
\item {64000 particles}
\item {many contacts}
\item {2400 time steps}
\item {8 hours}
\end{itemize}
\end{column}
\end{columns}
\end{frame}






\begin{frame}{}
\begin{block}{Perspective}
\begin{itemize}
\item temporal stability
\item Free surface / Deformable porous media
\item GPU
\item High order FE / Mesh convergence analysis
\item Physical applications
\end{itemize}
\end{block}
\begin{block}{Thanks}
\begin{itemize}
\item FNRS
\item Hydro Quebec
\end{itemize}
\end{block}
\pause
\begin{center}
{\Large Thank you for your attention}
\end{center}
\end{frame}
%
%\begin{frame}{
%\only<1>{We do not always have enough particles}
%\only<2>{It's even worse when we refine the mesh}
%\only<3>{Let's spread each particle}
%%\only<4>{We can refine the mesh}
%\only<4>{2-scales model : Intermediate scale}
%}
%\begin{center}
%{\frame{
%\includegraphics<1>[height=3.7cm]{fig/part2fe/ok.png}
%\includegraphics<2>[height=3.7cm]{fig/part2fe/bad_small.png}
%\includegraphics<3>[height=3.7cm]{fig/part2fe/bad_sample-1.png}
%%\includegraphics<4>[height=3.7cm]{fig/part2fe/bad_fixed.png}
%\includegraphics<4>[height=3.7cm]{fig/part2fe/bad_fixed_verry_small.png}}}
%\\
%\ \\
%\only<1-3> {{\frame{\includegraphics<1-3>[height=3.7cm]{fig/part2fe/bad.png}}}}
%\only<4>{
%\vspace*{-1cm}
%\begin{center}
%\begin{block}{}
%\begin{itemize}
%\item fluid scale $≠$ mesh scale
%\item mesh convergence analysis is possible
%\item not stricly necessary
%\end{itemize}
%\end{block}
%\end{center}
%}
%\end{center}
%\end{frame}
%
%\begin{frame}{Macro simulation validation : gradient of density}
%\end{frame}
\end{document}
