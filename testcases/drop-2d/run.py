import pyfefp
import scontact2
from pyfefp import scontact2Interface
import numpy as np
import time
import os

def genInitialPosition(filename, r, rout, rhop) :
    p = scontact2.ParticleProblem()
    scontact2Interface.MeshLoader(p, "mesh.msh", ["Top", "Box"])
    x = np.arange(rout, -rout, 2 * -r)
    x, y = np.meshgrid(x, x)
    R2 = x**2 + y**2
    keep = R2 < (rout - r)**2
    x = x[keep]
    y = y[keep]
    for i in range(x.shape[0]) :
        p.addParticle((x[i], y[i]), r, r**2 * np.pi * rhop);
    p.position()[:, 1] += 2.5
    p.write(filename)

dt = 0.025
tEnd = 15

outputdir = "output/"
if not os.path.isdir(outputdir) :
    os.makedirs(outputdir)

rhop=1100
d = 0.005
mu = 0.001
rho = 1000
ii = 0

filename = outputdir + "/part-00000"
genInitialPosition(filename, d, 0.4, rhop)

## scontact
p = scontact2Interface.ScontactInterface(outputdir+"/part-%05d"%ii)

## lmgc
#from pyfefp import lmgc2Interface
#lmgc2Interface.scontactToLmgc90(filename)
#p = lmgc2Interface.LmgcInterface()

g =  [0, -9.81]
print("r = %g, m = %g\n" %  (p.r()[0], p.mass()[0]))
print("dtmax = %g Re_p/u = %g" % (p.mass()[0] / (150 * mu * d), d * rho * 0.3/mu))
print("RHOP = %g" % rhop)
outf = 1
fluid = pyfefp.FluidSolver("mesh.msh", dt, rhop=rhop, rho=rho, g=g, mu=mu, imex=True)
fluid.add_boundary_condition("Box", 0, 0.)
fluid.add_boundary_condition("Box", 1, 0.)
fluid.add_boundary_condition("Top", 0, 0.)
fluid.add_boundary_condition("Top", 1, 0.)
fluid.add_boundary_condition("Top", 2, 0.)
fluid.complete()

t = 0
p.write(outputdir, ii, t)

fluid.set_particles(p.mass(), p.volume(), p.position(), p.velocity())
fluid.write_solution(outputdir, ii, t)

tic = time.clock()
while t < tEnd :
    forces = fluid.solve()
    vn = p.velocity() + forces * dt / p.mass()
    vmax = np.max(np.hypot(vn[:, 0], vn[:, 1]))
    nsub = max(1, int(np.ceil((vmax * dt * 2)/min(p.r()))))
    print("NSUB", nsub, "VMAX * dt", vmax * dt, "r", min(p.r()))
    for i in range(nsub) :
        p.iterate(dt/nsub, forces)
    fluid.set_particles(p.mass(), p.volume(), p.position(), p.velocity())
    t += dt
    if ii %outf == 0 :
        ioutput = int(ii/outf) + 1
        p.write(outputdir, ioutput, t)
        fluid.write_solution(outputdir, ioutput, t)
    ii += 1
    print("%i : %.2g/%.2g (cpu %.2g)" % (ii, t, tEnd, time.clock() - tic))
