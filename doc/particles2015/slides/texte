- title :
Good morning, I'm glad to present you our efforts to develop a model of mixture of fluid and grains flows.

- Fluid/Grains mixture model as any concentration
As you know, fluid and grains mixture are present in many industrial and geophysical applications.
Even at low grain concentration, they exhibit complex and inhomogeneous properties like clustering fingering or bubbling.
The dynamics of the fluid is strongly impacted by the presence of the grains, their concentration, their interaction with the fluid and the interactions between the grains themselves.

- Coupling fluid and particles
A broad range of methods have been developed to simulate such flows. 
On one side of the spectrum, we found fully resolved models where the fluid flow around each grain is explicitly computed by a direct Navier-Stokes simulation and a discrete element model is used for the solid grain. Those simulations are really expensive and are thus limited to relatively small domain of interest or representative volumes. 
On the other side, some large scale models represent the whole mixture as a single fluid with a specific rheology.

- (next)
Our model lays somewhere in-between.
We aim at simulating mixture with highly variable particle concentration and the interactions of those flows with a possibly complex surrounding structure. This requires a large domain able to capture the complexity of the structure and the inhomogeneity of the mixture.
That's why we choose an intermediate approach  where the movement and contacts of the solid grains are computed individually by a discrete element method while the fluid is solved at a coarser scale on a mesh whose elements are larger than the size of the grains.

- (next)
In addition, we perform a few direct simulation on smaller domains just to validate the two-scales approach.

- (nex)
Here are two example meshes. On the left, the mesh used for direct simulations and on the right side the one used in the two scales model. As you can see the mesh element size is several time larger than the size of the particles. Of course this approach is vastly cheaper but because each grain is represented explicitly, we still can take its shape and its orientation into account while computing its interaction with the surrounding fluid.

This work requires the coupling of different tools.

- Particle model : LMGC90
For the particle model, we use the LMGC90 software developed as the université de Montpellier which provides a general framework to simulate granular or divided media.

- Mesh Generator : GMSH
GMSH is used for the mesh generation. This is a software we develop the université catholique de Louvain and at the university of Liège in Belgium. It provides advanced mesh generation features like quad meshes, curved meshes or automatic surface reparameterization.

- Finite Element Solver
Finally our in-house finite element solver is used for the fluid which allow us to play with various temporal and spatial discretization.

- Direct simulations
Let's start with the simple direct simulations. Again this is not the main part of this work, its only purpose is to validate the two scale model. So we use a fine mesh and we solve the stokes or navier-stokes equations. The linear continuous shape functions  are not stable for this problem and we recourse to a classical solution which consists in adding an additional degree of freedom for the velocity on each element. Those degrees of freedom are associated with cubic shape functions with the shape of a bubble that vanish on the boundary. The advantage of this stabilisation is that it does not requires any parameter.

- (next)
This model correctly represents the pressure gradient as a function of the porosity. For various arrangement, we obtain a Darcy law with different permeability law as a function of the viscosity. For hexagonal arrangement, for square arrangement, and for a random distribution. In all case we follow the existing laws.

- 2-scales model : Average over a fluid control volume.
Let's move to the 2 scales model. To obtain the equations on the coarser scale of the fluid, the navier-stokes equations are integrated over a control volume. An indicator function is used to determine the volume occupied by the grains. The integral of this indicator function gives the porosity and its convolution with a field provides the volume average of this field. We use reynolds separation where each field is split into its value at the coarser scale and a variation. And finally the integral of the gradient of a fluid field can be expressed as the gradient of the mean value plus an integral of this field on the interface between the fluid and the grains. We can to the same thing for the divergence and those integrals allow us to express the forces between the fluid and the grains.

- Integrated Incompressible Navier-Stokes Equations.
By combining everything, we obtain the incompressible Navier-Stokes equations integrated on the control volume. Without surprise, in the volume conservation equation we can see that the divergence of the sum of the mean fluid and grain velocities is zero.
The momentum equation looks like the original equation where the transport and diffusion terms are slightly modified to take into account the variation of the porosity. More importantly, there is an additional term representing the forces between the fluid and the grains.
As we do not compute explicitly the flow around each grain, this term has to be parametrized.

- Parametrization of the particle-fluid forces
Different options exists to parameterize those forces. They can be evaluated at the integration point of the fluid model, using only mean fields. A Forccheimer term can be added to take into account the viscous drag. 
Alternatively the forces can be parametrized for each particle individually. This allows to take into account the shape and the orientation of each particle individually. In this case the fluid pressure velocity and pressure gradient have to be evaluated at the position of each particle. This is an example of parameterization but a whole zoology exists. The important aspect is that it depends on both the specific characteristic of the considered particles and the compacity which depends on all the surroundings particles. The contributions of all particles are then gathered on the fluid nodes. In practice, the results presented further will this formulation.
So we can choose among many parameterization but of course it's important to use the same formulation both in the grain model and in the fluid model. It's not consistant to compute the forces at the fluid scale in the fluid model and then use different small-scale parameterization at the particle scale.

- Finite Element formulation
Let's consider the simplest choice, the only force between the fluid and the grains is a Darcy term in which case the Navier-Stokes equation becomes a Brinkman equation.
When the permeability is close to one, the viscous term is dominant and the system reduces to a stokes problem. As soon as a significant amount of grains are present, the Darcy term becomes dominant and we have a Darcy problem. The nice thing is that both regimes can be captured by this single system. The bad thing is that the optimal choice of discretization space would be different. In the stokes regime, the pressure gradient is proportional to the second derivative of the velocity, so it would be better to use a discrete space of higher order for the velocity. But in the Darcy regime, a higher order space for the pressure would be optimal.
So a stabilisation is needed. We use the same trick as in the direct model, P1 basis function for the pressure and velocity field with an additional bubble shape function for the velocity.

- Particle <=> Mesh
We can easily project from the particle to the mesh and from the mesh to the particles, I do not think I have to explain that here. Let's just mention that the use of a lumped mass matrix, while slightly reducing the precision, allows us to avoid oscillation in the projection.

- Temporal coupling
The simplest possible temporal coupling is to solve alternatively the fluid and the particles. In this example, we ignore the advection in the fluid equation. And it works...

- (next)
this is a drop of particles falling in the fluid. While the results looks correct, the cpu time is not satisfactory. One hour is the time for a single threaded run on a regular laptop but still for this small 2d problem this is too slow.

- (next)
This is the porosity on the left and the pressure and velocity fields on the right

- (next)
the same test with more particles.
Here the cpu time becomes prohibitive 15 hours.
Again, the porosity, pressure and velocity fields.

- (next)
The problem is the number of required timestep twenty thousands for this simulation. 
The problem is that the explicit coupling introduces a very stringent stability condition even if both the solid and fluid model individually are stable for large time-steps

- (next)
To overrun this difficulty, is to substitute the solid velocity by an estimation of this velocity at the next time step. The simplest thing we can do is to replace the solid velocity at time n by the free solid velocity at time n plus one, the free velocity being the velocity that take into account the forces of the fluid on the particles but not the contacts between the particles.
This is stable but it's not consistant because free velocity is not an approximation of the velocity true velocity.

- (next)
But still, when there are few contacts like in this circular couette flows, it allows to reduces the computation time from 10 hours too 3 minutes without altering the solution.

- (next)
We are currently working on an fully implicit coupling. But the difficulty is to obtain a continuous approximation of the feedback of the small scale particles on the large scale fluid.

- Preliminary result : Pile
(launch the video immediately)
I will finish with this last simulation where a pile is pushed in a bed of particles saturated with water.
The whole domain is filled with water.

-(next)
Finally, the next steps is to solve properly the implicit coupling. We will add a free surface in the fluid, we are working on a GPU implementation. We would like to increase the order the fluid discretization and perform convergence analysis. That will require to define an intermediate scale at which the projection are done that will be coarser that the scale of the fluid mesh.

Thank you for your attention. 


