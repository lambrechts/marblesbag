from . import dofManager
import numpy as np

class ParticleFluid :
    def __init__(self, jac) :
        self.jac = jac
        self.porosity = jac.dofManager.new_vector()
        self.compacity = jac.dofManager.new_vector()
        self.mesh = jac.dofManager._mesh
        dofmanager2 = dofManager.DofManager(self.mesh)
        for i in range(self.jac.dim) :
            dofmanager2.add_field(jac.dofManager.get_field_basis(0))
        dofmanager2.complete()
        self.momentum = dofmanager2.new_vector()

    def set_particles(self, position, volume, velocity, forces, mass) :
        self.position = position
        self.volume = volume
        self.velocity = velocity
        self.forces = forces
        self.mass = mass
        els = self.jac.x
        if self.jac.dim == 2 :
            import scontact2
            self.eid, self.uvw = scontact2.particleToMesh(position, els)
        else :
            import scontact3
            self.eid, self.uvw = scontact3.particleToMesh(position, els)
        self.psi = self.jac.dofManager.get_field_basis(0).f(self.uvw)
        dof = self.jac.dofManager
        vertexVolume = dof.new_vector()
        if self.jac.dim == 2 :
            np.add.at(vertexVolume, dof._fields[0][0], np.outer([1./3, 1./3, 1./3], self.jac.J/2))
        else :
            np.add.at(vertexVolume, dof._fields[0][0], np.outer([1./4, 1./4, 1./4, 1./4], self.jac.J/4))
        vollocal = vertexVolume[dof._fields[0][0][:,self.eid]]
        self.compacity[:] = 0
        np.add.at(self.compacity, dof._fields[0][0][:, self.eid] , (self.psi * volume/vollocal.T).T)
        self.porosity[:] = 1 - self.compacity
        self.momentum[:] = 0
        for i in range(self.jac.dim) :
            np.add.at(self.momentum, self.momentum.dof_manager()._fields[i][0][:, self.eid] , (self.psi * velocity[:, [i]] * volume/vollocal.T).T)


