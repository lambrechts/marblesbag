from . import dofManager
from . import legendre
import numpy as np
import scipy.sparse

class MeshJacobian :
    def update(self, x) :
        self.x = x
        dim = self.dim
        self.dxdxi = np.ndarray((len(self.els), dim, dim))
        for i in range (dim) :
            self.dxdxi[:, :, i] = self.x[:, i + 1, :] - self.x[:, 0, :]
        self.J = np.linalg.det(self.dxdxi)
        self.dxidx = np.linalg.inv(self.dxdxi)
        #integ = integrationMatrices.integrationMatrices(dgpy.dgIntegrationMatrices.get(dgpy.MSH_TRI_3, 3))
        #psipsijw = np.einsum("nm, e -> enm", np.sum(integ.psiPsiW, axis = 0), self.J)
        #jrows = np.ndarray((len(d.getElements(0)), 3, 3))
        #jcols = np.ndarray((len(d.getElements(0)), 3, 3))
        #for i in range(3) :
        #    jrows[:, :, i] = d._fields[0][0].T
        #    jcols[:, i, :] = d._fields[0][0].T
        #self.mass = scipy.sparse.csr_matrix((psipsijw.flat, (jrows.flat, jcols.flat)))

    def writeScalar(self, filename, fieldname, it, t, v) :
        els = self.els
        n = v.shape[0]
        N = self.dim + 1
        with open(filename, "w") as f :
            f.write("$MeshFormat\n2.1 0 8\n$EndMeshFormat\n")
            f.write("$ElementNodeData\n1\n\"%s\"\n1\n%.16g\n3\n%i\n%i\n%i\n" % (fieldname, t, it, n/N, len(els)));
            form = "%i %i" + " %.16g" * n + "\n"
            for i in range(v.shape[1]) :
                f.write(form % ((els[i].tag, N) + tuple(v[:, i])))
            f.write("$EndElementNodeData\n")

    def writeVector(self, filename, fieldname, it, t, v) :
        els = self.els
        n = v.shape[0]
        with open(filename, "w") as f :
            f.write("$MeshFormat\n2.1 0 8\n$EndMeshFormat\n")
            f.write("$ElementNodeData\n1\n\"%s\"\n1\n%.16g\n3\n%i\n%i\n%i\n" % (fieldname, t, it, n/(self.dim + 1), len(els)));
            form = "%i %i" + " %.16g" * n + "\n"
            for i in range(v.shape[1]) :
                f.write(form % ((els[i].tag, n) + tuple(v[:, i])))
            f.write("$EndElementNodeData\n")

    #first axes is supposed to be the elements
    def multJ(self, x, els = slice(None)) :
        return x * self.J[(slice(None),) + (None,) * (len(x.shape) - 1)]

    def multDXiDXLeft(self, x, axis, els = slice(None)) :
        naxes = len(x.shape)
        xfront = (slice(None),) * axis + (np.newaxis,)
        xback = (slice(None),) * (naxes - axis - 1)
        jfront = (els,) + (np.newaxis,) * (axis - 1) + (slice(None),)
        jback = (np.newaxis,) * (naxes - axis - 1)
        X = x[xfront + (0,) + xback] * self.dxidx[jfront + (0,) + jback]
        for i in range(1,self.dim) :
            X += x[xfront + (i,) + xback] * self.dxidx[jfront + (i,) + jback]
        return X

    def multDXiDXRight(self, x, axis, els = slice(None)) :
        naxes = len(x.shape)
        xfront = (slice(None),) * axis
        xback = (np.newaxis,) + (slice(None),) * (naxes - axis - 1)
        jfront = (els,) + (np.newaxis,) * (axis - 1)
        jback =  (slice(None),) + (np.newaxis,) * (naxes - axis - 1)
        X = x[xfront + (0,) + xback] * self.dxidx[jfront + (0,) + jback]
        for i in range(1,self.dim) :
            X += x[xfront + (i,) + xback] * self.dxidx[jfront + (i,) + jback]
        return X


    def __init__(self, mesh, dim = None) :
        if not dim :
            dim = max([e.dimension for e in mesh.entities])
        d = dofManager.DofManager(mesh)
        self.dofManager = d
        self.dim = dim
        if self.dim == 3 :
            d.add_field(legendre.TetrahedronP1)
        else :
            d.add_field(legendre.TriangleP1)
        d.complete()
        self.mesh = mesh
        self.els = d.getElements(0)
        self.update(np.array([[i[:dim] for i in el.vertices] for el in self.els]))
