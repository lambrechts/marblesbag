#!/usr/bin/env python
import scontact2
import numpy as np
import mesh
import os

meshBoundary = True
outputdir = "output_depot"
rout = 0.0254
rin = 0.0064
r = 397e-6/2
compacity3d = 0.18

def genInitialPosition(p, N, r, rout, rin, rhop) :
    x = np.arange(rout, -rout, 2 * -r)
    x, y = np.meshgrid(x, x)
    R2 = x**2 + y**2
    keep = np.logical_and(R2 < (rout - r)**2,  R2 > (rin + r) **2)
    x = x[keep][:N]
    y = y[keep][:N]
    for i in range(N) :
        p.addParticle((x[i], y[i]), r, r**2 * np.pi * rhop);

addv = set()
def loadMeshBoundary(p, filename) :
    m = mesh.mesh(filename)
    for ent in m.entities :
        if ent.dimension != 1 :
            continue
        for i, el in enumerate(ent.elements) :
            for v in el.vertices :
                if v[3] in addv :
                    continue
                addv.add(v[3])
                p.addBoundaryDisk((v[0], v[1]), 0.000, 0)
            v0 = el.vertices[1]
            v1 = el.vertices[0]
            p.addBoundarySegment((v0[0], v0[1]), (v1[0], v1[1]), 0)

N = int((rout**2 - rin**2) * compacity3d * 3./2 / r**2)
surfacedense = N * 2 * np.sqrt(3) * r**2
surfacebulk = 0.34 * np.pi * (rout**2 - rin**2)
p = scontact2.ParticleProblem()
if meshBoundary :
    loadMeshBoundary(p, "disk.msh")
else :
    p.addBoundaryDisk((0, 0), rin, 0)
    p.addBoundaryDisk((0, 0), -rout, 0)
genInitialPosition(p, N, r, rout, rin, rhop=1.18e3)


scale = (surfacebulk/surfacedense)**0.5
print("total compacity 3d (0.2) = %g\n"  % (N * r**2 * 2./3 / (rout**2 - rin**2)))
print("scale = %g (%g %g)" % (scale, surfacedense, surfacebulk))
r *= scale

g = 1
tEnd = 3 * (4 * 0.0254 / g)**0.5;
t = 0
i = 0
ioutput = 1
os.makedirs(outputdir, exist_ok=True)
while t < tEnd :
    p.externalForces()[:, :] = - 3 * p.velocity()
    p.externalForces()[:, 1] += -g
    p.externalForces()[:, :] *= p.particles()[:, [1]]
    dt = min(0.001, p.maxdt(r)/2)
    p.iterate(r, dt, tol=1e-5)
    t += dt
    i += 1
    print("%.2g %.2g" % (t, tEnd))
    if i %100 == 0 :
        p.write("%s/part-%05i" % (outputdir, ioutput))
        ioutput += 1
p.write("deposited")
