#ifndef _SCONTACT_H_
#define _SCONTACT_H_
#include <stdlib.h>

typedef struct _ParticleProblem ParticleProblem;

ParticleProblem *particleProblemNew();
void particleProblemDelete(ParticleProblem *p);
void particleProblemLoad(ParticleProblem *p, const char *filename);
void particleProblemWrite(const ParticleProblem *p, const char *filename);
void particleProblemIterate(ParticleProblem *p, double alert, double dt, double tol, int maxit);
void particleProblemSolve(ParticleProblem *p, double alert, double dt, double tol, int maxit);
double particleProblemMaxDt(const ParticleProblem *p, double alert);
void particleProblemAddParticle(ParticleProblem *p, const double x[DIMENSION], double r, double m);
size_t particleProblemAddBoundaryDisk(ParticleProblem *p, const double x0[DIMENSION], double r, const char *tag);
size_t particleProblemAddBoundarySegment(ParticleProblem *p, const double x0[DIMENSION], const double x1[DIMENSION], const char *tag);
size_t particleProblemGetTagId(ParticleProblem *p, const char *tag);
const char* particleProblemGetTagName(ParticleProblem *p, size_t tag);
#if DIMENSION==3
void particleProblemAddBoundaryTriangle(ParticleProblem *p, const double x0[DIMENSION], const double x1[DIMENSION], const double x2[DIMENSION], const char *tag);
#endif
double *particleProblemDisk(ParticleProblem *p);
double *particleProblemSegment(ParticleProblem *p);
double *particleProblemParticle(ParticleProblem *p);
double *particleProblemVelocity(ParticleProblem *p);
double *particleProblemPosition(ParticleProblem *p);
double *particleProblemExternalForces(ParticleProblem *p);
unsigned long int particleProblemNParticle(ParticleProblem *p);
void particleProblemUseJacobi(ParticleProblem *p, int jacobi, double relax);
void particleProblemWriteVtk(ParticleProblem *p, const char *filename);
void particleProblemWriteBoundaryVtk(ParticleProblem *p, const char *filename);
double *particleProblemRadius(ParticleProblem *p);
int *particleProblemDiskTag(ParticleProblem *p);
int *particleProblemSegmentTag(ParticleProblem *p);
#if DIMENSION == 3
int *particleProblemTriangleTag(ParticleProblem *p);
double *particleProblemTriangle(ParticleProblem *p);
#endif
void ParticleToMesh(size_t nParticles, double *particles, size_t nElements, double *elements, size_t *elid, double *xi);
#endif
