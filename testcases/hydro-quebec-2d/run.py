import pyfefp
import numpy as np
import time
import os

class MeshVelocity :
    def __init__(self) :
        self.position = 0
        self.velocity = 0

    def call(self, x) :
        l = 0.1
        L = 2
        h = 0.5
        H = 1.5
        fx = np.minimum(1 - (np.abs(x[..., 0]) - l) / (L - l), 1.)
        HX = self.position * fx
        HH = np.where(x[..., 1] > HX, H - HX , H + HX)
        fy = np.minimum(1 - (np.abs(x[..., 1] - HX) - h) / (HH - h), 1.)
        v = np.zeros_like(x)
        v[..., 1] = fx * fy * self.velocity
        return v

tEnd = 0.1
dt = 0.001

outputdir = "output_run_%.10g_%.10g"%(tEnd, dt)
if not os.path.isdir(outputdir) :
    os.makedirs(outputdir)

mv = MeshVelocity()

# LMGC
#from pyfefp import lmgc2Interface
#lmgc2Interface.scontactToLmgc90("deposited")
#p = lmgc2Interface.LmgcInterface()

# SCONTACT
from pyfefp import scontact2Interface
p = scontact2Interface.ScontactInterface("deposited")

rhop = np.mean(p.mass() / p.volume())
fluid = pyfefp.FluidSolver("mesh.msh", dt, rhop=rhop, rho=1000, mu=1, g=[0, -9.81], imex=True)
fluid.add_boundary_condition("Pile", 0, cb = lambda x : mv.call(x)[:, 0])
fluid.add_boundary_condition("Pile", 1, cb = lambda x : mv.call(x)[:, 1])
fluid.add_boundary_condition("Box", 0, 0.)
fluid.add_boundary_condition("Box", 1, 0.)
fluid.add_boundary_condition("Top", 0, 0.)
fluid.add_boundary_condition("Top", 1, 0.)
fluid.add_boundary_condition("Top", 2, 0.)
fluid.complete()

mv.velocity = 0.5
fluid.set_mesh_position(fluid.mesh_position() + mv.call(fluid.mesh_position()))
mv.position = 0.5

t = 0
ii = 0
mv.velocity = -1/tEnd
p.setBoundaryVelocity("Pile", [0, mv.velocity])

fluid.set_particles(p.mass(), p.volume(), p.position(), p.velocity())
p.write_vtk(outputdir, ii, t)
p.write_boundary_vtk(outputdir, ii, t)
fluid.write_solution(outputdir, ii, t, "vtk")

tic = time.clock()

while t < tEnd :
    forces = fluid.solve()
    p.iterate(dt, forces)
    fluid.set_mesh_position(fluid.mesh_position() + mv.call(fluid.mesh_position()) * dt)
    fluid.set_particles(p.mass(), p.volume(), p.position(), p.velocity())
    mv.position += dt * mv.velocity
    t += dt
    ii += 1
    p.write_vtk(outputdir, ii, t)
    p.write_boundary_vtk(outputdir, ii, t)
    fluid.write_solution(outputdir, ii, t, "vtk")
    print("%i : %.2g/%.2g" % (ii, t, tEnd))
