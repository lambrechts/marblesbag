rin = 0.0064;
rout = 0.0254;
lcin = rout/20; 
lcout = rout/20;

Point(1) = {0, 0, 0};

/* Inner Circle */
Point(10) = {rin, 0, 0, lcin};
Point(11) = {0, rin, 0, lcin};
Point(12) = {-rin, 0, 0, lcin};
Point(13) = {0, -rin, 0, lcin};

Circle(10)={10, 1, 11};
Circle(11)={11, 1, 12};
Circle(12)={12, 1, 13};
Circle(13)={13, 1, 10};

Line Loop(10) = {10, 11, 12, 13};
Physical Line("Inner") = {10, 11, 12, 13};

/* Outer Circle */
Point(20) = {rout, 0, 0, lcout};
Point(21) = {0, rout, 0, lcout};
Point(22) = {-rout, 0, 0, lcout};
Point(23) = {0, -rout, 0, lcout};

Circle(20)={20, 1, 21};
Circle(21)={21, 1, 22};
Circle(22)={22, 1, 23};
Circle(23)={23, 1, 20};

Line Loop(20) = {20, 21, 22, 23};
Physical Line("Outer") = {20, 21, 22, 23};

/* Plane Surface */
Plane Surface(1) = {20, 10};
Physical Surface("Domain") = {1};
Physical Point("PtFix") = {13};
