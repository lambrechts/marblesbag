#ifndef _VECTOR_H_
#define _VECTOR_H_
#include <string.h>
#include <stdlib.h>

static size_t _vectorSize(void *m) {
  return m == NULL ? 0 : (*((size_t*)m - 1));
}

static void _vectorFree(void **m) {
  if (*m != NULL)
    free(((size_t *)*m) - 2);
  *m = NULL;
}
static void *_vectorPush(void **m, size_t s) {
  if (*m == NULL) {
    size_t *n = (size_t*)malloc(s * 2 + 2 * sizeof(size_t));
    n[0] = 2 * s;
    n[1] = s;
    *m = n + 2;
    return *m;
  }
  size_t *n = (*(size_t**)m) - 2;
  n[1] += s;
  if (n[0] < n[1]) {
    n[0] *= 2;
    n = (size_t*)realloc(n, n[0] + 2 * sizeof(size_t));
    *m= n + 2;
  }
  return ((char*) *m) + n[1] - s;
}

static void *_vectorInsert(void **m, size_t p, size_t s) {
  _vectorPush(m, s);
  memmove(((char*)*m) + p + s, ((char*)*m) + p, _vectorSize(*m) - s - p);
  return ((char*)*m) + p;
}

static void *_vectorDup(void *m) {
  if (m == NULL)
    return NULL;
  size_t *n = ((size_t*)m - 2);
  size_t N = n[1];
  size_t *a = (size_t*) malloc(sizeof(size_t) * 2 + N);
  memcpy(a, n, sizeof(size_t) * 2 + N);
  a[0] = a[1] = N;
  return a + 2;
}
static void vectorClear(void *m) {
  if (m != NULL)
    *(((size_t*) m) - 1) = 0;
}
static void _vectorPop(void *m, size_t s) {
  if (m != NULL) {
    *((size_t*)m - 1)-= s;
  }
}
#define vectorSize(v) (_vectorSize((void*)v)/sizeof(*v))
#define vectorPush(v) ((__typeof__(*v))_vectorPush((void**)v, sizeof(**v)))
#define vectorPushN(v, x) ((__typeof__(*v))_vectorPush((void**)v, sizeof(**v) * (x)))
#define vectorInsert(v, p) ((__typeof__(*v))_vectorInsert((void**)v, p * (sizeof(**v)), sizeof(**v)))
#define vectorPop(v) _vectorPop((void*)v, sizeof(*v))
#define vectorPopN(v, x) _vectorPop((void*)v, sizeof(*v) * (x)))
#define vectorFree(m) _vectorFree((void**)&m)
#define vectorDup(m) ((__typeof__(m))_vectorDup(m))
#endif
