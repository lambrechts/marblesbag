import numpy as np
import shutil
import scontact2
from . import mesh
class ScontactInterface :
    def __init__(self, inputfile) :
        p = scontact2.ParticleProblem()
        self._p = p
        p.load(inputfile)
        p.velocity()[:, :] = 0;
        self._r = p.particles()[:, 0]

        self._volume = np.pi * p.particles()[:, [0]]**2 * 2./3 ## 2./3 = 2d-3d correction
        self._mass = p.particles()[:, [1]]
        self._position = p.position()
        self._velocity = p.velocity()

    def volume(self):
        return self._volume

    def r(self) :
        return self._r

    def mass(self):
        return self._mass

    def position(self):
        return self._position

    def velocity(self):
        return self._velocity

    def setBoundaryVelocity(self, tag, v) :
        p = self._p
        tag = p.getTagId(tag)
        p.disks()[p.diskTag() == tag, 2:4] = v
        p.segments()[p.segmentTag() == tag, 4:6] = v

    def iterate(self, dt, forces) :
        self._p.externalForces()[:] = forces
        self._p.iterate(np.min(self._r), dt, 1e-5)

    def write(self, odir, i, t) :
        outputname = "%s/part-%05i" % (odir, i)
        self._p.write(outputname + "_tmp")
        shutil.move(outputname + "_tmp", outputname)

    def write_vtk(self, odir, i, t) :
        outputname = "%s/part-%05i.vtp" % (odir, i)
        self._p.writeVtk(outputname + "_tmp")
        shutil.move(outputname + "_tmp", outputname)

    def write_boundary_vtk(self, odir, i, t) :
        outputname = "%s/boundary-%05i.vtp" % (odir, i)
        self._p.writeBoundaryVtk(outputname + "_tmp")
        shutil.move(outputname + "_tmp", outputname)



class MeshLoader(object) :

    def __init__(self, p, filename, tags = None, shift = [0, 0]) :
        self._p = p
        self._m = mesh.Mesh(filename)
        self._addv = set()
        if tags is not None :
            self.load(tags, shift)

    def load(self, tags, shift= [0, 0]) :
        pnum = [self._m.getPhysicalNumber(1, i) for i in tags]
        for ent in self._m.entities :
            if ent.dimension != 1 or not [i for i in pnum if i in ent.physicals]:
                continue
            tag = 0 if not ent.physicals else ent.physicals[0]
            stag = self._m.getPhysicalName(ent.dimension, tag) or str(tag)
            for i, el in enumerate(ent.elements) :
                for v in el.vertices :
                    if v[3] in self._addv :
                        continue
                    self._addv.add(v[3])
                    self._p.addBoundaryDisk((v[0] + shift[0], v[1] + shift[1]), 0.000, stag)
                v0 = el.vertices[1]
                v1 = el.vertices[0]
                self._p.addBoundarySegment((v0[0] + shift[0], v0[1] + shift[1]), (v1[0] + shift[0], v1[1] + shift[1]), stag)
