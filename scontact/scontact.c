#include "scontact.h" 
#include "quadtree.h"
#include "vector.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <string.h>

typedef struct _Particle Particle;
typedef struct _Contact Contact;
typedef struct _Disk Disk;
typedef struct _Segment Segment;
typedef struct _Triangle Triangle;

struct _ParticleProblem{
  int jacobi;
  double relax;
  Particle *particles;
  Contact *contacts;
  double *position, *velocity, *externalForces;
  Disk *disks;
  char **tagname;
  int *diskTag, *segmentTag;
  Segment *segments;
  Contact *contactParticleDisks, *contactParticleSegments;
  #if DIMENSION == 3
  Triangle *triangles;
  Contact *contactParticleTriangles;
  int *triangleTag;
  #endif
};

static double dot(const double *x, const double *y) {
  #if DIMENSION == 3
  return x[0] * y[0] +  x[1] * y[1] + x[2] * y[2];
  #else
  return x[0] * y[0] +  x[1] * y[1];
  #endif
}

void coordRead(FILE *f, double *x) {
  for (int i = 0; i < DIMENSION; ++i)
    fscanf(f, "%le", &x[i]);
}

void coordWrite(FILE *f, double *x) {
  for (int i = 0; i < DIMENSION; ++i)
    fprintf(f, " %.16g", x[i]);
}

/* Particle */
struct _Particle{
  double r;
  double m;
};

struct _Contact {
  size_t o0, o1;
  double a0, a1;
  double D;
  double dv;
  double n[DIMENSION];
};

static void particleBoundingBox(const Particle *p, const double x[DIMENSION], double pmin[DIMENSION], double pmax[DIMENSION]){
  for (int i = 0; i < DIMENSION; ++i) {
    pmin[i] = x[i] - p->r;
    pmax[i] = x[i] + p->r;
  }
}

static int particleInitContact(size_t id0, Particle *p0, double *x0, size_t id1, Particle *p1, double *x1, double alert, Contact *c) {
  double nnorm = 0;
  c->dv = 0;
  c->o0 = id0;
  c->o1 = id1;
  for (int k = 0; k < DIMENSION; ++k) {
    c->n[k] = x0[k] - x1[k];
    nnorm += c->n[k] * c->n[k];
  }
  nnorm = sqrt(nnorm);
  for (int i = 0; i < DIMENSION; ++i)
    c->n[i] /= -nnorm;
  c->D = fmax(0., nnorm - (p0->r + p1->r));
  c->a0 = p1->m / (p0->m + p1->m);
  c->a1 = p0->m / (p0->m + p1->m);
  return c->D < alert;
}

struct _Disk {
  double x[DIMENSION];
  double v[DIMENSION];
  double r;
};

static void diskBoundingBox(const Disk *d, double *pmin, double *pmax) {
  const double r = fabs(d->r);
  for (int i = 0; i < DIMENSION; ++i) {
    pmin[i] = d->x[i] - r;
    pmax[i] = d->x[i] + r;
  }
}

static size_t particleProblemAddBoundaryDiskTagId(ParticleProblem *p, const double x[DIMENSION], double r, size_t tag) {
  Disk *d = vectorPush(&p->disks);
  *vectorPush(&p->diskTag) = tag;
  d->r = r;
  for (int i = 0; i < DIMENSION; ++i) {
    d->x[i] = x[i];
    d->v[i] = 0.;
  }
  return vectorSize(p->disks) - 1;
}

size_t particleProblemAddBoundaryDisk(ParticleProblem *p, const double x[DIMENSION], double r, const char *tag) {
  return particleProblemAddBoundaryDiskTagId(p, x, r, particleProblemGetTagId(p, tag));
}

static int diskInitContact(size_t id, const Disk *d, size_t particle, const Particle *p, double *x, double alert, Contact *c) {
  double nnorm = 0;
  c->dv = 0;
  c->o1 = particle;
  c->o0 = id;
  for (int i = 0; i < DIMENSION; ++i) {
    c->n[i] = d->x[i] - x[i];
    nnorm += c->n[i] * c->n[i];
  }
  nnorm = sqrt(nnorm);
  c->D = (nnorm - fabs(p->r + d->r)) * (d->r < 0 ? -1 : 1);
  if (c->D < 0) c->D = 0;
  for (int i = 0; i < DIMENSION; ++i) {
    c->n[i] /= nnorm * (d->r < 0 ? -1 : 1);
  }
  c->a0 = 0.;
  c->a1 = 1.;
  return c->D < alert;
}

struct _Segment{
  double p[2][DIMENSION];
  double v[DIMENSION];
};

static void segmentBoundingBox(const Segment *s, double *pmin, double *pmax) {
  for (int i = 0; i < DIMENSION; ++i) {
    pmin[i] = fmin(s->p[0][i], s->p[1][i]);
    pmax[i] = fmax(s->p[0][i], s->p[1][i]);
  }
}

static int segmentProjectionIsInside(const Segment *s, double *x) {
  double alpha = 0, beta = 0;
  for (int i = 0; i < DIMENSION; ++i) {
    const double d = s->p[0][i] - s->p[1][i];
    alpha += (x[i] - s->p[0][i]) * d;
    beta  += (x[i] - s->p[1][i]) * d;
  }
  return (alpha < 0 && beta > 0);
}

static int segmentInitContact(size_t id, const Segment *s, size_t particle, const Particle *p, double *x, double alert, Contact *c) {
  c->o0 = id;
  c->o1 = particle;
  c->dv = 0;
  double t[DIMENSION];
  double nt2 = 0;
  double dt = 0;
  for (int i = 0; i <DIMENSION; ++i){
    t[i] = s->p[1][i] - s->p[0][i];
    dt += t[i] * (s->p[0][i] - x[i]);
    nt2 += t[i] * t[i];
  }
  double nn2 = 0;
  for (int i = 0; i < DIMENSION; ++i) {
    c->n[i] = s->p[0][i] - x[i] - t[i] / nt2 *  dt;
    nn2 += c->n[i] * c->n[i];
  }
  const double nnorm = sqrt(nn2);
  for (int i = 0; i < DIMENSION; ++i) {
    c->n[i] /= nnorm;
  }
  c->D = nnorm - p->r;
  if (c->D < 0 && segmentProjectionIsInside(s, x)) c->D = 0;
  c->a0 = 0.;
  c->a1 = 1.;
  return c->D >= 0  && c->D < alert;
}

#if DIMENSION == 3
struct _Triangle {
  double p[3][DIMENSION];
  double v[DIMENSION];
};

static void particleProblemAddBoundaryTriangleTagId(ParticleProblem *p, const double x0[3], const double x1[3], const double x2[3], size_t tag) {
  Triangle *t = vectorPush(&p->triangles);
  *vectorPush(&p->triangleTag) = tag;
  for (int i = 0; i < DIMENSION; ++i) {
    t->p[0][i] = x0[i];
    t->p[1][i] = x1[i];
    t->p[2][i] = x2[i];
    t->v[i] = 0.;
  }
}

void particleProblemAddBoundaryTriangle(ParticleProblem *p, const double x0[3], const double x1[3], const double x2[3], const char *tag) {
  return particleProblemAddBoundaryTriangleTagId(p, x0, x1, x2, particleProblemGetTagId(p, tag));
}

static void triangleBoundingBox(const Triangle *t, double *pmin, double *pmax) {
  for (int i = 0; i < 3; ++i) {
    pmin[i] = fmin(fmin(t->p[0][i], t->p[1][i]), t->p[2][i]);
    pmax[i] = fmax(fmax(t->p[0][i], t->p[1][i]), t->p[2][i]);
  }
}

static void _cross (const double *a, const double *b, double *c) {
  c[0] = a[1] * b[2] - a[2] * b[1];
  c[1] = a[2] * b[0] - a[0] * b[2];
  c[2] = a[0] * b[1] - a[1] * b[0];
}

static int triangleProjectionIsInside(const Triangle *t, const double *x) {
  double d0[3] = {t->p[1][0] - t->p[0][0], t->p[1][1] - t->p[0][1], t->p[1][2] - t->p[0][2]};
  double d1[3] = {t->p[2][0] - t->p[0][0], t->p[2][1] - t->p[0][1], t->p[2][2] - t->p[0][2]};
  double n[3];
  _cross(d0, d1, n);
  double xp[3];
  double dp[3] = {t->p[0][0] - x[0], t->p[0][1] - x[1], t->p[0][2] - x[2]};
  const double nd = dot(n, dp);
  for (int i = 0; i < 3; ++i) 
    xp[i] = x[i] + n[i] * nd;
  double dx[3][3] = 
  {{xp[0] - t->p[0][0], xp[1] - t->p[0][1], xp[2] - t->p[0][2]},
    {xp[0] - t->p[1][0], xp[1] - t->p[1][1], xp[2] - t->p[1][2]},
    {xp[0] - t->p[2][0], xp[1] - t->p[2][1], xp[2] - t->p[2][2]}};
  double alpha[3];
  for (int i = 0; i < 3; ++i) {
    double d[3];
    _cross(dx[(i + 1) % 3], dx[(i + 2) %3], d);
    alpha[i] = 0.5 * dot(n, d);
  }
  return ((alpha[0] <= 0 && alpha[1] <= 0 &&  alpha[2]<= 0) || (alpha[0] >= 0 && alpha[1] >= 0 && alpha[2] >= 0));
}

static int triangleInitContact(size_t id, const Triangle *t, size_t particle, const Particle *p, double *x, double alert, Contact *c) {
  c->o0 = id;
  c->o1 = particle;
  c->dv = 0;
  double d0[3] = {t->p[1][0] - t->p[0][0], t->p[1][1] - t->p[0][1], t->p[1][2] - t->p[0][2]};
  double d1[3] = {t->p[2][0] - t->p[0][0], t->p[2][1] - t->p[0][1], t->p[2][2] - t->p[0][2]};
  double N[3];
  _cross(d0, d1, N);
  const double nn = sqrt(dot(N, N));
  for (int i = 0; i < 3; ++i)
    c->n[i] = N[i] / nn;
  double dd[3] = {t->p[0][0] - x[0], t->p[0][1] - x[1], t->p[0][2] - x[2]};
  c->D = dot(c->n, dd);
  if (c->D < 0) {
    for (int i = 0; i <3; ++i)
      c->n[i] = -c->n[i];
    c->D = -c->D;
  }
  c->D -= p->r;
  c->a0 = 0.;
  c->a1 = 1.;
  if (c->D < 0)
    c->D = 0;
  return (c->D > 0 || triangleProjectionIsInside(t, x)) && c->D < alert;
}

#endif

static double contactParticleParticleSolve(Contact *c, const double *v0, const double *v1, double dt) {
  #if DIMENSION == 3
  double vn = (v0[0] - v1[0]) * c->n[0] + (v0[1] - v1[1]) * c->n[1] + (v0[2] - v1[2]) * c->n[2] + c->dv;
  #else 
  double vn = (v0[0] - v1[0]) * c->n[0] + (v0[1] - v1[1]) * c->n[1] + c->dv;
  #endif
  return fmax(0., vn - c->D/dt);
}

static double contactParticleBndSolve(Contact *c, const double *v, double vlocfree[DIMENSION], double *vn, double dt) {
  for (int i = 0; i < DIMENSION; ++i)
    vlocfree[i] = v[i] + c->n[i] * c->dv;
  *vn = dot(vlocfree, c->n);
  return fmax(0, *vn - c->D/dt);
}

static void coordAdd(double *x, double a, const double *y) { // x += a * y
  x[0] += a * y[0];
  x[1] += a * y[1];
  #if DIMENSION == 3
  x[2] += a * y[2];
  #endif
}


Contact *findContactSorted(Contact *c, Contact *v, size_t *i) {
  while (*i < vectorSize(v) && (v[*i].o0 < c->o0 || (v[*i].o0 == c->o0 && v[*i].o1 < c->o1)))
    (*i)++;
  return (*i < vectorSize(v) && v[*i].o0 == c->o0 && v[*i].o1 == c->o1) ? &v[*i] : NULL;
}

static void addAlert(double alert, double *rmin, double *rmax) {
  rmin[0] -= alert;
  rmin[1] -= alert;
  rmax[0] += alert;
  rmax[1] += alert;
  #if DIMENSION == 3
  rmin[2] -= alert;
  rmax[2] += alert;
  #endif
}

static void bbadd(double *amin, double *amax, const double *bmin, const double *bmax)
{
  for (int i = 0; i < DIMENSION; ++i) {
    if (bmin[i] < amin[i])
      amin[i] = bmin[i];
    if (bmax[i] > amax[i])
      amax[i] = bmax[i];
  }
}

static void _particleProblemBoundingBox(ParticleProblem *p, double *bbmin, double *bbmax) {
  double amin[DIMENSION], amax[DIMENSION];
  for (int i = 0; i < DIMENSION; ++i) {
    bbmin[i] = DBL_MAX;
    bbmax[i] = -DBL_MAX;
  }
  for (size_t i = 0; i < vectorSize(p->particles); ++i) {
    particleBoundingBox(&p->particles[i], &p->position[i * DIMENSION], amin, amax);
    bbadd(bbmin, bbmax, amin, amax);
  }
  for (size_t i = 0; i < vectorSize(p->disks); ++i) {
    diskBoundingBox(&p->disks[i], amin, amax);
    bbadd(bbmin, bbmax, amin, amax);
  }
  for (size_t i = 0; i < vectorSize(p->segments); ++i) {
    segmentBoundingBox(&p->segments[i], amin, amax);
    bbadd(bbmin, bbmax, amin, amax);
  }
  #if DIMENSION == 3
  for (size_t i = 0; i < vectorSize(p->triangles); ++i) {
    triangleBoundingBox(&p->triangles[i], amin, amax);
    bbadd(bbmin, bbmax, amin, amax);
  }
  #endif
}

static void _bboxtol(double *bbmin, double *bbmax) {
  double lmax = 0;
  for (int i = 0; i < DIMENSION; ++i) {
    lmax = fmax(lmax, bbmax[i] - bbmin[i]);
  }
  double tol = 1e-8 * lmax;
  for (int i = 0; i < DIMENSION; ++i) {
    bbmax[i] += tol;
    bbmin[i] -= tol;
  }
}


double volTet(double x0[3], double x1[3], double x2[3], double x3[3]) {
  // x1 * (y2 * (z3 - z4) - z2 * (y3 - y4) + y3 * z4 - y4 * z3)
  //- y1 * (x2 * (z3 - z4) - z2 * (x3 - x4) + x3 * z4 - x4 * z3)
  //+ z1 * (x2 * (y3 - y4) - y2 * (x3 - x4) + x3 * y4 - x4 * y3)
  //- (x2 * (y3 * z4 - y4 * z3) - y2 * (x3 * z4 - x4 * z3) + z2 * (x3 * y4 - x4 * y3))
  return  x0[0] * (x1[1] * (x2[2] - x3[2]) - x1[2] * (x2[1] - x3[1]) + x2[1] * x3[2] - x3[1] * x2[2])
    - x0[1] * (x1[0] * (x2[2] - x3[2]) - x1[2] * (x2[0] - x3[0]) + x2[0] * x3[2] - x3[0] * x2[2])
    + x0[2] * (x1[0] * (x2[1] - x3[1]) - x1[1] * (x2[0] - x3[0]) + x2[0] * x3[1] - x3[0] * x2[1])
    - (x1[0] * (x2[1] * x3[2] - x3[1] * x2[2]) - x1[1] * (x2[0] * x3[2] - x3[0] * x2[2]) + x1[2] * (x2[0] * x3[1] - x3[0] * x2[1]));
}

void ParticleToMesh(size_t nParticles, double *particles, size_t nElements, double *elements, size_t *elid, double *Xi)
{
  double bbmin[DIMENSION], bbmax[DIMENSION];
  int N = DIMENSION + 1;
  for (int i = 0; i < DIMENSION; ++i) {
    bbmin[i] = elements[i];
    bbmax[i] = elements[i];
  }
  for (size_t i = 0; i < N * nElements; ++i) {
    for (int d = 0; d < DIMENSION; ++d) {
      bbmin[d] = fmin(bbmin[d], elements[DIMENSION * i + d]);
      bbmax[d] = fmax(bbmax[d], elements[DIMENSION * i + d]);
    }
  }
  _bboxtol(bbmin, bbmax);
  Cell *tree = cellNew(bbmin, bbmax);
  double amin[DIMENSION], amax[DIMENSION];
  for (size_t i = 0; i < nElements; ++i) {
    double *el = elements + (DIMENSION * N) * i;
    for (int d = 0; d < DIMENSION; ++d) {
      amin[d] = el[d];
      amax[d] = el[d];
    }
    for (int v = 1; v < N; ++v) {//only simplices
      for (int d = 0; d < DIMENSION; ++d) {
        amin[d] = fmin(amin[d], el[v * DIMENSION + d]);
        amax[d] = fmax(amax[d], el[v * DIMENSION + d]);
      }
    }
    _bboxtol(amin, amax);
    cellAdd(tree, amin, amax, i, NULL);
  }
  int *found = NULL;
  vectorPushN(&found, 100);
  vectorClear(found);
  for (size_t i = 0; i < nParticles; ++i) {
    double *x = &particles[i * DIMENSION];
    elid[i] = -1;
    cellSearch(tree, x, x, &found);
    for (size_t j = 0; j < vectorSize(found); ++j) {
      double *el = &elements[found[j] * N * DIMENSION];
      if (DIMENSION == 2)  {
        double *X[3] = {el, el + DIMENSION, el + DIMENSION * 2};
        double dx = x[0] - X[0][0], dy = x[1] - X[0][1];
        double DX[2] = {X[1][0] - X[0][0], X[2][0] - X[0][0]};
        double DY[2] = {X[1][1] - X[0][1], X[2][1] - X[0][1]};
        double det = DX[1] * DY[0] - DX[0] * DY[1];
        double xi  = (DX[1] * dy - DY[1] * dx) / det;
        double eta = -(DX[0] * dy - DY[0] * dx) / det;
        if (xi > -1e-8 && eta > -1e-8 && 1 - xi - eta > -1e-8) {
          Xi[i * DIMENSION + 0] = xi;
          Xi[i * DIMENSION + 1] = eta;
          elid[i] = found[j];
          break;
        }
      }
      else {
        double *X[4] = {el, el + DIMENSION, el + DIMENSION * 2, el + DIMENSION * 3};
        double v = volTet(X[0], X[1], X[2], X[3]);
        double v0 = volTet(x, X[1], X[2], X[3]);
        if (v0 * v < -1e-8)
          continue;
        double v1 = volTet(X[0], x, X[2], X[3]);
        if (v1 * v < -1e-8)
          continue;
        double v2 = volTet(X[0], X[1], x, X[3]);
        if (v2 * v < -1e-8)
          continue;
        double v3 = volTet(X[0], X[1], X[2], x);
        if (v3 * v < -1e-8)
          continue;
        Xi[i * 3 + 0] = v1 / v;
        Xi[i * 3 + 1] = v2 / v;
        Xi[i * 3 + 2] = v3 / v;
        elid[i] = found[j];
        break;
      }
    }
    /*if (elid[i] == -1) {
      printf(" PARTICLE %i OUTSIDE DOMAIN N2 search!!!\n", i);
      double toll = -1000;
      for (size_t j = 0; j < nElements; ++j) {
        double *el = &elements[j * N * DIMENSION];
        double *X[3] = {el, el + DIMENSION, el + DIMENSION * 2};
        double dx = x[0] - X[0][0], dy = x[1] - X[0][1];
        double DX[2] = {X[1][0] - X[0][0], X[2][0] - X[0][0]};
        double DY[2] = {X[1][1] - X[0][1], X[2][1] - X[0][1]};
        double det = DX[1] * DY[0] - DX[0] * DY[1];
        double xi  = (DX[1] * dy - DY[1] * dx) / det;
        double eta = -(DX[0] * dy - DY[0] * dx) / det;
        toll = fmax(toll, fmin(-xi, fmin(-eta, xi + eta -1)));
        if (xi > -1e-8 && eta > -1e-8 && 1 - xi - eta > -1e-8) {
          Xi[i * DIMENSION + 0] = xi;
          Xi[i * DIMENSION + 1] = eta;
          elid[i] = j;
          break;
        }
      }*/
      if (elid[i] == -1) {
        printf(" PARTICLE %lu OUTSIDE DOMAIN!!! %g %g\n", i, x[0], x[1]);
        exit(1);
      }
    //}
  }
  cellFree(tree);
}

static void _particleProblemGenContacts(ParticleProblem *p, const double alert)
{
  size_t iold = 0;
  double bbmin[DIMENSION], bbmax[DIMENSION];
  _particleProblemBoundingBox(p, bbmin, bbmax);
  Cell *tree = cellNew(bbmin, bbmax);
  double amin[DIMENSION], amax[DIMENSION];
  int *found = NULL;
  vectorPushN(&found, 100);
  vectorClear(found);
  // Particles
  Contact *oldContacts = vectorDup(p->contacts), *cold;
  vectorClear(p->contacts);
  for (size_t i = 0; i < vectorSize(p->particles); ++i) {
    particleBoundingBox(&p->particles[i], &p->position[i * DIMENSION], amin, amax);
    addAlert(alert/2, amin, amax);
    vectorClear(found);
    cellAdd(tree, amin, amax, i, &found);
    for (int j = 0; j < vectorSize(found); ++j) {
      Contact *c= vectorPush(&p->contacts);
      if(!particleInitContact(i, &p->particles[i], &p->position[i * DIMENSION], found[j], &p->particles[found[j]], &p->position[found[j] * DIMENSION], alert, c))
        vectorPop(p->contacts);
      else if ((cold = findContactSorted(c, oldContacts, &iold))) {
        coordAdd(&p->velocity[c->o0 * DIMENSION], -cold->dv * c->a0, c->n);
        coordAdd(&p->velocity[c->o1 * DIMENSION], cold->dv * c->a1, c->n);
        c->dv = cold->dv;
      }
    }
  }
  vectorFree(oldContacts);
  // Disks
  oldContacts = vectorDup(p->contactParticleDisks);
  iold = 0;
  vectorClear(p->contactParticleDisks);
  for (size_t i = 0; i < vectorSize(p->disks); ++i) {
    diskBoundingBox(&p->disks[i], amin, amax);
    addAlert(alert/2, amin, amax);
    vectorClear(found);
    cellSearch(tree, amin, amax, &found);
    for (int j = 0; j < vectorSize(found); ++j) {
      Contact *c = vectorPush(&p->contactParticleDisks);
      if(!diskInitContact(i, &p->disks[i], found[j], &p->particles[found[j]], &p->position[found[j] * DIMENSION], alert, c))
        vectorPop(p->contactParticleDisks);
      else if ((cold = findContactSorted(c, oldContacts, &iold))) {
        coordAdd(&p->velocity[c->o1 * DIMENSION], -cold->dv, c->n);
       c->dv = cold->dv;
      }
    }
  }
  vectorFree(oldContacts);
  // Segments
  oldContacts = vectorDup(p->contactParticleSegments);
  iold = 0;
  vectorClear(p->contactParticleSegments);
  for (size_t i = 0; i < vectorSize(p->segments); ++i) {
    segmentBoundingBox(&p->segments[i], amin, amax);
    addAlert(alert/2, amin, amax);
    vectorClear(found);
    cellSearch(tree, amin, amax, &found);
    for (int j = 0; j < vectorSize(found); ++j) {
      Contact *c = vectorPush(&p->contactParticleSegments);
      if (!segmentInitContact(i, &p->segments[i], found[j], &p->particles[found[j]], &p->position[found[j] * DIMENSION], alert, c))
        vectorPop(p->contactParticleSegments);
      else if ((cold = findContactSorted(c, oldContacts, &iold))) {
        coordAdd(&p->velocity[c->o1 * DIMENSION], -cold->dv, c->n);
        c->dv = cold->dv;
      }
    }
  }
  vectorFree(oldContacts);
  // Triangles 
  #if DIMENSION == 3
  oldContacts = vectorDup(p->contactParticleTriangles);
  iold = 0;
  vectorClear(p->contactParticleTriangles);
  for (size_t i = 0; i < vectorSize(p->triangles); ++i) {
    triangleBoundingBox(&p->triangles[i], amin, amax);
    addAlert(alert/2, amin, amax);
    vectorClear(found);
    cellSearch(tree, amin, amax, &found);
    for (int j = 0; j < vectorSize(found); ++j) {
      Contact *c =  vectorPush(&p->contactParticleTriangles);
      if (!triangleInitContact(i, &p->triangles[i], found[j], &p->particles[found[j]], &p->position[found[j] * DIMENSION], alert, c))
        vectorPop(p->contactParticleTriangles);
      else if ((cold = findContactSorted(c, oldContacts, &iold))) {
        coordAdd(&p->velocity[c->o1 * DIMENSION], -cold->dv, c->n);
        c->dv = cold->dv;
      }
    }
  }
  vectorFree(oldContacts);
  #endif
  vectorFree(found);
  cellFree(tree);
}

static void _particleProblemSolveContacts(ParticleProblem *p, double dt, double tol, int maxit) {
  int iter = 0;
  for (double residual = DBL_MAX; residual > tol/dt && (maxit < 0 || iter < maxit);) {
    residual = 0;
    iter++;
    double *velocityNew =  p->velocity;
    if (p->jacobi)
      velocityNew = vectorDup(p->velocity);
    // particle - particle
    for (size_t i = 0; i < vectorSize(p->contacts); ++i) {
      Contact *c = &p->contacts[i];
      double dp = contactParticleParticleSolve(&p->contacts[i], &p->velocity[c->o0 * DIMENSION], &p->velocity[c->o1 * DIMENSION], dt) - c->dv;
      residual = fmax(fabs(dp), residual);
      dp *= p->relax;
      coordAdd(&velocityNew[c->o0 * DIMENSION], -dp * c->a0, c->n);
      coordAdd(&velocityNew[c->o1 * DIMENSION], dp * c->a1, c->n);
      c->dv += dp;
    }
    // particle - triangle
    #if DIMENSION == 3
    for (size_t ii = 0; ii <vectorSize(p->contactParticleTriangles); ++ii) {
      Contact *c = &p->contactParticleTriangles[ii];
      double vlocfree[DIMENSION], vn, xn[DIMENSION], vloc[DIMENSION];
      for (int i= 0; i < DIMENSION; ++i)
        vloc[i] = p->velocity[c->o1 * DIMENSION + i] - p->triangles[c->o0].v[i];
      double pp = contactParticleBndSolve(c, vloc, vlocfree, &vn, dt);
      for (int i = 0; i < DIMENSION; ++i)
        xn[i] = p->position[c->o1 * DIMENSION + i] + p->particles[c->o1].r * c->n[i] + vlocfree[i] * c->D/vn;
      if (!triangleProjectionIsInside(&p->triangles[c->o0], xn))
        pp = 0;
      double dp = pp - c->dv;
      residual = fmax(fabs(dp), residual);
      dp *= p->relax;
      coordAdd(&velocityNew[c->o1 * DIMENSION], -dp, c->n);
      c->dv += dp;
    }
    #endif
    // particle - segment
    for (size_t ii = 0; ii < vectorSize(p->contactParticleSegments); ++ii) {
      Contact *c = &p->contactParticleSegments[ii];
      double vlocfree[DIMENSION], vn, xn[DIMENSION], vloc[DIMENSION];
      for (int i= 0; i < DIMENSION; ++i)
        vloc[i] = p->velocity[c->o1 * DIMENSION + i] - p->segments[c->o0].v[i];
      double pp = contactParticleBndSolve(c, vloc, vlocfree, &vn, dt);
      for (int i = 0; i < DIMENSION; ++i)
        xn[i] = p->position[c->o1 * DIMENSION + i] + p->particles[c->o1].r * c->n[i] + vlocfree[i] * c->D / vn;
      if (!segmentProjectionIsInside(&p->segments[c->o0], xn))
        pp = 0;
      double dp = pp - c->dv;
      residual = fmax(fabs(dp), residual);
      dp *= p->relax;
      coordAdd(&velocityNew[c->o1 * DIMENSION], -dp, c->n);
      c->dv += dp;
    }
    // particle - disk
    for (size_t ii = 0; ii < vectorSize(p->contactParticleDisks); ++ii) {
      Contact *c = &p->contactParticleDisks[ii];
      double vlocfree[DIMENSION], vn;
      double vloc[DIMENSION];
      for (int i= 0; i < DIMENSION; ++i)
        vloc[i] = p->velocity[c->o1 * DIMENSION + i] - p->disks[c->o0].v[i];
      double dp = contactParticleBndSolve(c, vloc, vlocfree, &vn, dt) - c->dv;
      residual = fmax(fabs(dp), residual);
      dp *= p->relax;
      coordAdd(&velocityNew[c->o1 * DIMENSION], -dp, c->n);
      c->dv += dp;
    }
    if (p->jacobi) {
      for (size_t i = 0; i < vectorSize(p->velocity); ++i)
        p->velocity[i] = velocityNew[i];
      vectorFree(velocityNew);
    }
  }
  printf("iter = %i\n", iter);
}

double particleProblemMaxDt(const ParticleProblem *p, double alert) {
  double VM2 = 0;
  double FM2 = 0;
  for (size_t j = 0; j < vectorSize(p->particles); ++j) {
    VM2 = fmax(VM2, dot(&p->velocity[j * DIMENSION], &p->velocity[j * DIMENSION]));
    FM2 = fmax(FM2, dot(&p->externalForces[j * DIMENSION], &p->externalForces[j * DIMENSION]) / (p->particles[j].m * p->particles[j].m));
  }
  const double VM = sqrt(VM2);
  const double FM = sqrt(FM2);
  const double q = VM + sqrt(VM2 + 4 * FM * alert);
  return alert / q;
}

void particleProblemSolve(ParticleProblem *p, double alert, double dt, double tol, int maxit)
{
  _particleProblemGenContacts(p, alert);
  _particleProblemSolveContacts(p, dt, tol, maxit);
}

void particleProblemIterate(ParticleProblem *p, double alert, double dt, double tol, int maxit)
{
  for (size_t j = 0; j < vectorSize(p->particles); ++j)
    for (size_t i = 0; i < DIMENSION; ++i)
      p->velocity[j * DIMENSION + i] += p->externalForces[j * DIMENSION + i] * dt / p->particles[j].m;
  particleProblemSolve(p, alert, dt, tol, maxit);
  for (size_t i = 0; i < vectorSize(p->position); ++i)
    p->position[i] += p->velocity[i] * dt;
  for (size_t i = 0; i < vectorSize(p->disks); ++i)
    for (int j = 0; j < DIMENSION; ++j)
      p->disks[i].x[j] += p->disks[i].v[j] * dt;
  for (size_t i = 0; i < vectorSize(p->segments); ++i)
    for (int j = 0; j < DIMENSION; ++j) {
      p->segments[i].p[0][j] += p->segments[i].v[j] * dt;
      p->segments[i].p[1][j] += p->segments[i].v[j] * dt;
    }
  #if DIMENSION == 3
  for (size_t i = 0; i < vectorSize(p->triangles); ++i)
    for (int j = 0; j < DIMENSION; ++j) {
      p->triangles[i].p[0][j] += p->triangles[i].v[j] * dt;
      p->triangles[i].p[1][j] += p->triangles[i].v[j] * dt;
      p->triangles[i].p[2][j] += p->triangles[i].v[j] * dt;
    }
  #endif
}

static size_t particleProblemAddBoundarySegmentTagId(ParticleProblem *p, const double x0[DIMENSION], const double x1[DIMENSION], size_t tag) {
  Segment *s = vectorPush(&p->segments);
  *vectorPush(&p->segmentTag) = tag;
  for (int i = 0; i < DIMENSION; ++i) {
    s->p[0][i] = x0[i];
    s->p[1][i] = x1[i];
    s->v[i] = 0.;
  }
  return vectorSize(p->segments) - 1;
}

size_t particleProblemAddBoundarySegment(ParticleProblem *p, const double x0[DIMENSION], const double x1[DIMENSION], const char *tag) {
  return particleProblemAddBoundarySegmentTagId(p, x0, x1, particleProblemGetTagId(p, tag));
}

void particleProblemAddParticle(ParticleProblem *p, const double x[DIMENSION], double r, double m) {
  size_t n = vectorSize(p->particles);
  Particle *particle = vectorPush(&p->particles);
  particle->r = r;
  particle->m = m;
  vectorPushN(&p->position, DIMENSION);
  vectorPushN(&p->velocity, DIMENSION);
  vectorPushN(&p->externalForces, DIMENSION);
  for (int i = 0; i < DIMENSION; ++i) {
    p->position[n * DIMENSION +  i] = x[i];
    p->velocity[n * DIMENSION +  i] = 0;
    p->externalForces[n * DIMENSION + i] = 0;
  }
}

void particleProblemWrite(const ParticleProblem *p, const char *filename) {
  FILE *output = fopen(filename, "w");
  fprintf(output, "DIMENSION %i\n", DIMENSION);
  fprintf(output, "TAGS %lu", vectorSize(p->tagname));
  for (size_t i = 0; i < vectorSize(p->tagname); ++i) {
    fprintf(output, " %s", p->tagname[i]);
  }
  fprintf(output, "\n");
  #if DIMENSION == 3
  for (size_t i = 0; i < vectorSize(p->triangles); ++i) {
    fprintf(output, "T");
    coordWrite(output, p->triangles[i].p[0]);
    coordWrite(output, p->triangles[i].p[1]);
    coordWrite(output, p->triangles[i].p[2]);
    fprintf(output, " %i\n", p->triangleTag[i]);
  }
  #endif
  for (size_t i = 0; i < vectorSize(p->segments); ++i){
    fprintf(output, "S");
    coordWrite(output, p->segments[i].p[0]);
    coordWrite(output, p->segments[i].p[1]);
    fprintf(output, " %i\n", p->segmentTag[i]);
  }
  for (size_t i = 0; i < vectorSize(p->disks); ++i) {
    fprintf(output, "D");
    coordWrite(output, p->disks[i].x);
    fprintf(output, " %.16g %i\n", p->disks[i].r, p->diskTag[i]);
  }
  for(size_t i = 0; i<  vectorSize(p->particles); ++i) {
    fprintf(output, "P");
    coordWrite(output, &p->position[i * DIMENSION]);
    coordWrite(output, &p->velocity[i * DIMENSION]);
    fprintf(output, " %.16g %.16g\n", p->particles[i].r, p->particles[i].m);
  }
  fclose(output);
}

void particleProblemWriteVtk(ParticleProblem *p, const char *filename) {
  FILE *f = fopen(filename, "w");
  fprintf(f, "<VTKFile type=\"PolyData\">\n");
  fprintf(f, "<PolyData>\n");
  fprintf(f, "<Piece NumberOfPoints=\"%lu\">\n", vectorSize(p->particles));
  fprintf(f, "<PointData Scalars=\"Radius\" Vectors=\"Velocity\">\n");
  fprintf(f, "<DataArray Name=\"Radius\" NumberOfComponents = \"1\" type=\"Float32\" format=\"ascii\">\n");
  for (int i = 0; i < vectorSize(p->particles); ++i) {
    fprintf(f, "%.16g ", p->particles[i].r);
  }
  fprintf(f, "\n</DataArray>\n");
  fprintf(f, "<DataArray Name=\"Velocity\" NumberOfComponents = \"3\" type=\"Float32\" format=\"ascii\">\n");
  for (int i = 0; i < vectorSize(p->particles); ++i) {
#if DIMENSION == 2
    fprintf(f, "%.16g %.16g 0 ", p->velocity[i * 2], p->velocity[i * 2 + 1]);
#else
    fprintf(f, "%.16g %.16g %.16g ", p->velocity[i * 3], p->velocity[i * 3 + 1], p->velocity[i * 3 + 2]);
#endif
  }
  fprintf(f, "\n</DataArray>\n");
  fprintf(f, "</PointData>\n");
  fprintf(f, "<Points>\n<DataArray NumberOfComponents=\"3\" type=\"Float32\" format=\"ascii\">\n");
  for (int i = 0; i < vectorSize(p->position); ++i) {
#if DIMENSION == 2
    fprintf(f, "%.16g %.16g 0 ", p->position[i * 2], p->position[i * 2 + 1]);
#else
    fprintf(f, "%.16g %.16g %.16g ", p->position[i * 3], p->position[i * 3 + 1], p->position[i * 3 + 2]);
#endif
  }
  fprintf(f, "\n</DataArray>\n</Points>\n");
  fprintf(f, "</Piece>\n");
  fprintf(f, "</PolyData>\n");
  fprintf(f, "</VTKFile>\n");
  fclose(f);
}

void particleProblemWriteBoundaryVtk(ParticleProblem *p, const char *filename) {
  FILE *f = fopen(filename, "w");
  fprintf(f, "<VTKFile type=\"PolyData\">\n");
  fprintf(f, "<PolyData>\n");
  fprintf(f, "<Piece NumberOfPoints=\"%lu\" NumberOfLines=\"%lu\">\n", vectorSize(p->segments)*2, vectorSize(p->segments));
  fprintf(f, "<Points>\n<DataArray NumberOfComponents=\"3\" type=\"Float32\" format=\"ascii\">\n");
  for (int i = 0; i < vectorSize(p->segments); ++i) {
    Segment *s = &p->segments[i];
    fprintf(f, "%.16g %.16g %.16g ", s->p[0][0], s->p[0][1], DIMENSION == 2 ? 0. : s->p[0][2]);
    fprintf(f, "%.16g %.16g %.16g ", s->p[1][0], s->p[1][1], DIMENSION == 2 ? 0. : s->p[1][2]);
  }
  fprintf(f, "\n</DataArray>\n");
  fprintf(f, "</Points>\n");
  fprintf(f, "<Lines>\n");
  fprintf(f, "<DataArray type=\"Int32\" Name=\"connectivity\">\n");
  for (int i = 0; i < vectorSize(p->segments); ++i) {
    fprintf(f, "%i %i ", i*2, i*2+1);
  }
  fprintf(f, "\n</DataArray>\n");
  fprintf(f, "<DataArray type=\"Int32\" Name=\"offsets\">\n");
  for (int i = 0; i < vectorSize(p->segments); ++i) {
    fprintf(f, "%i ", (i+1)*2);
  }
  fprintf(f, "\n</DataArray>\n");
  fprintf(f, "</Lines>\n");
  fprintf(f, "</Piece>\n");
#if DIMENSION == 3
  fprintf(f, "<Piece NumberOfPoints=\"%lu\" NumberOfPolys=\"%lu\">\n", vectorSize(p->triangles)*3, vectorSize(p->triangles));
  fprintf(f, "<Points>\n<DataArray NumberOfComponents=\"3\" type=\"Float32\" format=\"ascii\">\n");
  for (int i = 0; i < vectorSize(p->triangles); ++i) {
    Triangle *t = &p->triangles[i];
    fprintf(f, "%.16g %.16g %.16g ", t->p[0][0], t->p[0][1], t->p[0][2]);
    fprintf(f, "%.16g %.16g %.16g ", t->p[1][0], t->p[1][1], t->p[1][2]);
    fprintf(f, "%.16g %.16g %.16g ", t->p[2][0], t->p[2][1], t->p[2][2]);
  }
  fprintf(f, "\n</DataArray>\n");
  fprintf(f, "</Points>\n");
  fprintf(f, "<Polys>\n");
  fprintf(f, "<DataArray type=\"Int32\" Name=\"connectivity\">\n");
  for (int i = 0; i < vectorSize(p->triangles)*3; ++i) {
    fprintf(f, "%i ", i);
  }
  fprintf(f, "\n</DataArray>\n");
  fprintf(f, "<DataArray type=\"Int32\" Name=\"offsets\">\n");
  for (int i = 0; i < vectorSize(p->triangles); ++i) {
    fprintf(f, "%i ", (i+1)*3);
  }
  fprintf(f, "\n</DataArray>\n");
  fprintf(f, "</Polys>\n");
  fprintf(f, "</Piece>\n");
#endif
  fprintf(f, "</PolyData>\n");
  fprintf(f, "</VTKFile>\n");
  fclose(f);
}

void particleProblemLoad(ParticleProblem *p, const char *filename)
{
  FILE *input = fopen(filename, "r");
  char type[128];
  int tag;
  char stag[256];
  while (fscanf(input, "%127s", type) == 1) {
    if (!strcmp(type, "DIMENSION")) {
      int dim;
      fscanf(input, "%d", &dim);
      if (dim != DIMENSION){
        printf("model in file \"%s\" is of dimension %i\n", filename, dim);
        exit(1);
      }
    }
    else if (!strcmp(type, "TAGS")) {
      for (size_t i = 0; i < vectorSize(p->tagname); ++i)
        free(p->tagname[i]);
      vectorFree(p->tagname);
      p->tagname = NULL;
      int n;
      fscanf(input, "%d", &n);
      for (size_t i = 0; i < n; ++i) {
        fscanf(input, "%255s", stag);
        if (strlen(stag) == 255) {
          printf("tag name cannot exceed 255 characters\n");
          exit(1);
        }
        particleProblemGetTagId(p, stag);
      }
    }
    else if (!strcmp(type, "P")) {
      double point[DIMENSION], v[DIMENSION];
      double r, m;
      coordRead(input, point);
      coordRead(input, v);
      fscanf(input, "%le %le", &r, &m);
      particleProblemAddParticle(p, point, r, m);
      for (int i = 0; i < DIMENSION; ++i)
        p->velocity[(vectorSize(p->particles) - 1) * DIMENSION +  i] = v[i];
    }
    else if (!strcmp(type, "D")) {
      double point[DIMENSION];
      double r;
      coordRead(input, point);
      fscanf(input, "%le %d", &r, &tag);
      particleProblemAddBoundaryDiskTagId(p, point, r, tag);
    }
    else if (!strcmp(type, "S")) {
      double x0[DIMENSION], x1[DIMENSION];
      coordRead(input, x0);
      coordRead(input, x1);
      fscanf(input, "%d", &tag);
      particleProblemAddBoundarySegmentTagId(p, x0, x1, tag);
    }
    #if DIMENSION == 3
    else if (!strcmp(type, "T")) {
      double x0[DIMENSION], x1[DIMENSION], x2[DIMENSION];
      coordRead(input, x0);
      coordRead(input, x1);
      coordRead(input, x2);
      fscanf(input, "%d", &tag);
      particleProblemAddBoundaryTriangleTagId(p, x0, x1, x2, tag);
    }
    #endif
    else {
      printf("unkown type \"%s\" in file \"%s\".\n", type, filename);
      exit(0);
    }
  }
}


int *particleProblemDiskTag(ParticleProblem *p) {return p->diskTag;}
int *particleProblemSegmentTag(ParticleProblem *p) {return p->segmentTag;};
#if DIMENSION == 3
int *particleProblemTriangleTag(ParticleProblem *p) {return p->triangleTag;};
double *particleProblemTriangle(ParticleProblem *p) {return (double*)&p->triangles[0];}
#endif
double *particleProblemVelocity(ParticleProblem *p) {return &p->velocity[0];}
double *particleProblemDisk(ParticleProblem *p) {return (double*)&p->disks[0];}
double *particleProblemSegment(ParticleProblem *p) {return (double*)&p->segments[0];}
double *particleProblemParticle(ParticleProblem *p) {return (double*)&p->particles[0];}
double *particleProblemPosition(ParticleProblem *p){return &p->position[0];}
double *particleProblemExternalForces(ParticleProblem *p){return &p->externalForces[0];}
void particleProblemUseJacobi(ParticleProblem *p, int jacobi, double relax) {p->jacobi = jacobi; p->relax = relax;}
void particleProblemDelete(ParticleProblem *p) {
  vectorFree(p->particles);
  vectorFree(p->disks);
  vectorFree(p->diskTag);
  vectorFree(p->segments);
  vectorFree(p->segmentTag);
  vectorFree(p->contactParticleDisks);
  vectorFree(p->contactParticleSegments);
  vectorFree(p->contacts);
  vectorFree(p->position);
  vectorFree(p->velocity);
  vectorFree(p->externalForces);
  #if DIMENSION == 3
  vectorFree(p->triangles);
  vectorFree(p->triangleTag);
  vectorFree(p->contactParticleTriangles);
  #endif
  for (size_t i = 0; i < vectorSize(p->tagname); ++i)
    free(p->tagname[i]);
  vectorFree(p->tagname);
  free(p);
}

size_t particleProblemGetTagId(ParticleProblem *p, const char *tag) {
  for (size_t i = 0; i < vectorSize(p->tagname); ++i)
    if (!strcmp(tag, p->tagname[i]))
      return i;
  char *dup = malloc(sizeof(char) * strlen(tag));
  strcpy(dup, tag);
  *vectorPush(&p->tagname) = dup;
  return vectorSize(p->tagname) - 1;
}

const char *particleProblemGetTagName(ParticleProblem *p, size_t id) {
  if (id >= vectorSize(p->tagname))
    return NULL;
  return p->tagname[id];
}

ParticleProblem *particleProblemNew() {
  ParticleProblem *p = (ParticleProblem*)malloc(sizeof(ParticleProblem));
  p->jacobi = 0;
  p->relax = 1.;
  p->particles = NULL;
  p->contacts = NULL;
  p->contactParticleDisks = NULL;
  p->tagname = NULL;
  p->contactParticleSegments = NULL;
  p->disks = NULL;
  p->diskTag = NULL;
  p->segments = NULL;
  p->segmentTag = NULL;
  p->position = NULL;
  p->velocity = NULL;
  p->externalForces = NULL;
  #if DIMENSION == 3
  p->triangles = NULL;
  p->triangleTag = NULL;
  p->contactParticleTriangles = NULL;
  #endif
  return p;
}

unsigned long int particleProblemNParticle(ParticleProblem *p) {
  return vectorSize(p->particles);
}

double *particleProblemRadius(ParticleProblem *p) {
  return (double*)p->particles;
}
