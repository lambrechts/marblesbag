import pyfefp
import numpy as np
import time
import os

class MeshVelocity :
    def __init__(self) :
        self.position = 0
        self.velocity = 0

    def cb(self, x) :
        l = 0.2
        L = 1
        h = 0.5
        H = 1.5
        R = np.hypot(x[..., 0], x[..., 1])
        fx = np.minimum(1 - (R - l) / (L - l), 1.)
        HX = self.position * fx
        HH = np.where(x[..., 2] > HX, H - HX , H + HX)
        fy = np.minimum(1 - (np.abs(x[..., 2] - HX) - h) / (HH - h), 1.)
        v = np.zeros_like(x)
        v[..., 2] = fx * fy * self.velocity
        return v

tCycle = 0.01
tEnd = tCycle * 3
dt = tCycle/200

outputdir = "output_run"
if not os.path.isdir(outputdir) :
    os.makedirs(outputdir)

mv = MeshVelocity()

# LMGC
#from pyfefp import lmgc3Interface
#lmgc3Interface.scontactToLmgc90("deposited")
#p = lmgc3Interface.LmgcInterface()

# SCONTACT
from pyfefp import scontact3Interface
p = scontact3Interface.ScontactInterface("deposited")

rhop = np.mean(p.mass() / p.volume())
fluid = pyfefp.FluidSolver("mesh.msh", dt, rhop=rhop, rho=1000, mu=1, g = [0, 0, -9.81], imex=True)
fluid.add_boundary_condition("Pile", 0, cb = lambda x : mv.cb(x)[:, 0])
fluid.add_boundary_condition("Pile", 1, cb = lambda x : mv.cb(x)[:, 1])
fluid.add_boundary_condition("Pile", 2, cb = lambda x : mv.cb(x)[:, 2])
fluid.add_boundary_condition("Box", 0, 0.)
fluid.add_boundary_condition("Box", 1, 0.)
fluid.add_boundary_condition("Box", 2, 0.)
fluid.add_boundary_condition("PtFix", 0, 0.)
fluid.add_boundary_condition("PtFix", 1, 0.)
fluid.add_boundary_condition("PtFix", 2, 0.)
fluid.add_boundary_condition("PtFix", 3, 0.)
fluid.complete()

mv.velocity = 0.5
fluid.set_mesh_position(fluid.mesh_position() + mv.cb(fluid.mesh_position()))
mv.position = 0.5

t = 0
ii = 0

fluid.set_particles(p.mass(), p.volume(), p.position(), p.velocity())
p.write(outputdir, ii, t)
fluid.write_solution(outputdir, ii, t)
fluid.write_position(outputdir, ii, t)

tic = time.clock()

while t < tEnd :
    mv.velocity = (-1 if ((t % tCycle) < (tCycle / 2)) else 1) * 1/ (tCycle/2)
    p.setBoundaryVelocity("Pile", [0, 0, mv.velocity])
    forces = fluid.solve()
    p.iterate(dt, forces)
    fluid.set_mesh_position(fluid.mesh_position() + mv.cb(fluid.mesh_position()) * dt)
    fluid.set_particles(p.mass(), p.volume(), p.position(), p.velocity())
    mv.position += dt * mv.velocity
    t += dt
    ii += 1
    p.write(outputdir, ii, t)
    fluid.write_solution(outputdir, ii, t)
    fluid.write_position(outputdir, ii, t)
    print("%i : %.2g/%.2g" % (ii, t, tEnd))
