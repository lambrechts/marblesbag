#include "ObjectCollection.h"
#include <algorithm>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <cfloat>
#include <png.h>
#include <GL/gl.h>
#include <stdarg.h>
#include <sys/stat.h>

static void rainbow(float f, float &r, float &g, float &b) {
  float  a=(1-f)/0.25;
  int  x =floor(a);
  float  y = a - x;
  switch(x)
  {
    case 0: r=1;g=y;b=0;break;
    case 1: r=1 -y ;g=1;b=0;break;
    case 2: r=0;g=1;b=y;break;
    case 3: r=0;g=1-y;b=1;break;
    case 4: r=0;g=0;b=1;break;
  }
}

class SolidSphere
{
  protected:
    std::vector<GLfloat> vertices;
    std::vector<GLfloat> normals;
    std::vector<GLushort> indices;

  public:
    SolidSphere(float radius, unsigned int rings, unsigned int sectors)
    {
      float const R = 1./(float)(rings-1);
      float const S = 1./(float)(sectors-1);
      int r, s;

      vertices.resize(rings * sectors * 3);
      normals.resize(rings * sectors * 3);
      std::vector<GLfloat>::iterator v = vertices.begin();
      std::vector<GLfloat>::iterator n = normals.begin();
      for(r = 0; r < rings; r++) for(s = 0; s < sectors; s++) {
        float const y = sin( -M_PI_2 + M_PI * r * R );
        float const x = cos(2*M_PI * s * S) * sin( M_PI * r * R );
        float const z = sin(2*M_PI * s * S) * sin( M_PI * r * R );

        *v++ = x * radius;
        *v++ = y * radius;
        *v++ = z * radius;

        *n++ = x;
        *n++ = y;
        *n++ = z;
      }

      indices.resize(rings * sectors * 4);
      std::vector<GLushort>::iterator i = indices.begin();
      for(r = 0; r < rings-1; r++) for(s = 0; s < sectors-1; s++) {
        *i++ = r * sectors + s;
        *i++ = r * sectors + (s+1);
        *i++ = (r+1) * sectors + (s+1);
        *i++ = (r+1) * sectors + s;
      }
    }

    void draw()
    {
      glEnableClientState(GL_VERTEX_ARRAY);
      glEnableClientState(GL_NORMAL_ARRAY);
      glVertexPointer(3, GL_FLOAT, 0, &vertices[0]);
      glNormalPointer(GL_FLOAT, 0, &normals[0]);
      glDrawElements(GL_QUADS, indices.size(), GL_UNSIGNED_SHORT, &indices[0]);
      glDisableClientState(GL_VERTEX_ARRAY);
      glDisableClientState(GL_NORMAL_ARRAY);
    }
};

void ObjectCollection::display() 
{
  glMatrixMode(GL_MODELVIEW);
  glScalef(1./_scale, 1./_scale, 1./_scale);
  glTranslatef(_shift[0], _shift[1], 0.);
  if (dim == 3){
    glRotatef(_rotation[1], 1, 0, 0);
    glRotatef(_rotation[0], 0, 0, 1);
  }
  glEnable(GL_COLOR_MATERIAL);
  if (dim == 3) {
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat mat_shininess[] = { 20 };
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
    glShadeModel (GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    GLfloat light_position[] = { -10.0, -10.0, 10.0, 0.0 };
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glClearColor(0., 0., 0., 0.);
  } else {
    glClearColor(1., 1., 1., 1.);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  }

  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  glColor4f(1, 0.5, 0, 1);
  glPushMatrix();
  double S = *std::max_element(radius.begin(), radius.end())/2;
  size_t zidx = 0;
  if (dim == 3) {
    SolidSphere sphere(S, 15, 15);
    for(size_t i = 0; i < radius.size(); ++i) {
      if (coord[i * 3 + 1] < 0 && _clip) continue;
      glPushMatrix();
      glTranslatef(coord[i * 3], coord[i * 3 + 1], coord[i * 3 + 2]);
      if (fixed[i]) glColor4f(1, 0.5, 0, 1);
      else {
        float aa = (coordorig[i *3 +2 ]+ 1.5)/(-0.35 +1.5);//(hypot(coordorig[i * 3 + 1], coordorig[i*3]));
        float r, g, b;
        rainbow(aa, r, g, b);
        glColor4f(r, g, b, 1.);
      }
      double f = radius[i]/S;
      glScalef(f, f, f);
      sphere.draw();
      glPopMatrix();
    }
  }else {
    for(size_t i = 0; i < radius.size(); ++i) {
      glEnableClientState(GL_VERTEX_ARRAY);
      glVertexPointer(2, GL_FLOAT, 0, &circleGeom[0]);
      glPushMatrix();
      glTranslatef(coord[i * 2], coord[i * 2 + 1], 0);
      glScalef(radius[i], radius[i], 1.);
      if(!fixed[i]) {
        glColor4f(0.2, 0.2, 0.6, 0.5);
        glDrawArrays(GL_TRIANGLE_FAN, 0, circleGeom.size() / 2);
      }
      glColor4f(0.4, 0.4, 0.8, 1);
      glDrawArrays(GL_LINE_STRIP, 1, circleGeom.size() / 2  -1);
      glDisableClientState(GL_VERTEX_ARRAY);
      glPopMatrix();
    }
  }
  glEnableClientState(GL_VERTEX_ARRAY);
  glColor4f(0., 1., 1., 1);
  glPolygonMode( GL_FRONT_AND_BACK, GL_FILL ); // GL_FILL
  glVertexPointer(dim, GL_FLOAT, 0, &tricoord[0]);
  glDrawArrays(GL_TRIANGLES, 0, tricoord.size() / dim);
  glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
  glDisable(GL_LIGHTING);
  glDisable(GL_LIGHT0);
  glColor4f(0.4, 0.4, 0.8, 1);
  glVertexPointer(dim, GL_FLOAT, 0, &segcoord[0]);
  glDrawArrays(GL_LINES, 0, segcoord.size() / dim);
  glDisableClientState(GL_VERTEX_ARRAY);
  glPopMatrix();
  if (_ffmpeg) {
    int xywh[4];
    glGetIntegerv(GL_VIEWPORT, xywh);
    int s = xywh[2] * xywh[3];
    int *data = new int[s];
    glReadPixels(xywh[0], xywh[1], xywh[2], xywh[3], GL_RGBA, GL_UNSIGNED_BYTE, data);
    fwrite(data, sizeof(int) * s, 1, _ffmpeg);
    delete []data;
  }
}


static void readCoord(std::istream &stream, std::vector<float> &coordVector, int size)
{
  for (int i = 0; i < size; ++i) {
    double x;
    stream >> x;
    coordVector.push_back(x);
  }
}

const std::string ObjectCollection::getTagName(size_t id) const {
  if (id >= _tagname.size())
    return "";
  return _tagname[id];
}

int ObjectCollection::read(int step) {
  if (step == -1)
    step = _step;
  std::ostringstream oss;
  oss << _basename << "/part-" <<std::setw(5)<<std::setfill('0')<< step;
  _filename = oss.str();
  std::ifstream hf(_filename.c_str());
  if (!hf.is_open())
    return 0;
  _step = step;
  radius.clear();
  coord.clear();
  segcoord.clear();
  tricoord.clear();
  std::string type;
  std::vector<float> vel;
  dim = -1;
  int tag;
  while (hf>>type) {
    if (type == "DIMENSION") {
      hf >> dim;
      continue;
    }
    if (dim == -1) {
      std::cout <<"Dimension not specified in file \""<<_filename<<"\"\n";
      exit(0);
    }
    if (type == "TAGS") {
      size_t n;
      hf >> n;
      _tagname.clear();
      for (int i = 0; i < n; ++i) {
        std::string buf;
        hf >> buf;
        _tagname.push_back(buf);
      }
      continue;
    }
    if (type == "P") {
      readCoord(hf, coord, 1 * dim);
      readCoord(hf, vel, 1 * dim);
      double r, m;
      hf >> r >> m;
      radius.push_back(r);
      fixed.push_back(false);
    }
    else if (type == "D") {
      readCoord(hf, coord, 1 * dim);
      double r;
      hf >> r >> tag;
      radius.push_back(r);
      fixed.push_back(true);
    }
    else if (type == "S"){
      readCoord(hf, segcoord, 2 * dim);
      hf >> tag;
      size_t ii = segcoord.size() - 2 * dim;
      if((_visible_flag[tag] & 1) || (_clip && (segcoord[ii + 1] + segcoord[ii + dim + 1])/2 < 0))
        segcoord.resize(segcoord.size() - 2 * dim);
    }
    else if (type == "T"){
      readCoord(hf, tricoord, 3 * dim);
      hf >> tag;
      size_t ii = tricoord.size() - 3 * dim;
      if((_visible_flag[tag] & 2) || (_clip && (tricoord[ii + 1] + tricoord[ii + dim + 1] + tricoord[ii + 2 * dim + 1])/2 < 0))
        tricoord.resize(tricoord.size() - 3 * dim);
    }
    else {
      std::cout <<"Unknown type \""<< type << "\"in file \""<<_filename<<"\"\n";
      exit(0);
    }
  }
  if (coordorig.empty() && dim == 3)
    coordorig = coord;


  if (_scale == -1){
    float bbox[6];
    _compute_bbox(bbox);
    _scale = std::max(bbox[1] - bbox[0], std::max(bbox[3] - bbox[2], bbox[5] - bbox[4]))/1.8;
  }
  return 1;
}

void ObjectCollection::save_opt(const char *filename) {
  FILE *f = fopen(filename, "w");
  fprintf(f, "alpha %.16g\nbeta %1.6g\nscale %.16g\nshift %.16g %.16g\n", _rotation[0], _rotation[1], _scale, _shift[0], _shift[1]);
  fprintf(f, "visible %lu", _visible_flag.size());
  for (std::map<int, int>::iterator  it = _visible_flag.begin(); it != _visible_flag.end(); ++it) {
    fprintf(f, " %i %i", it->first, it->second);
  }
  fprintf(f, "\n");
  fprintf(f, "clip %i\n", _clip ? 1 : 0);
  fclose(f);
}

void ObjectCollection::load_opt(const char *filename) {
  std::ifstream f(filename);
  if (!f) {
    std::cerr << "warning : cannot open file \""<<  filename << "\"\n";
    return;
  }
  std::string word;
  while (f>>word) {
    if (word == "alpha") f >> _rotation[0];
    else if (word == "beta") f >> _rotation[1];
    else if (word == "scale") f >> _scale;
    else if (word == "shift") f >> _shift[0] >> _shift[1];
    else if (word == "clip") f >> _clip;
    else if (word == "visible") {
      size_t count;
      f>> count;
      for (size_t i = 0; i< count; ++i) {
        int a, b;
        f >> a >>b;
        _visible_flag[a] = b;
      }
    }
    else {
      std::cerr << "warning : unknown option \""<<  word << "\"\n";
      f.ignore(30000, '\n');
    }
  }
}

void ObjectCollection::shift(double dx, double dy) {
  _shift[0] += dx * _scale;
  _shift[1] += dy * _scale;
}

void ObjectCollection::rotate(double r0, double r1) {
  _rotation[0] += r0;
  _rotation[1] += r1;
}

void ObjectCollection::zoom_at(double f, double cx, double cy) {
  shift(-cx * (1 - f), cy * (1 - f));
  _scale *= f;
}

bool ObjectCollection::file_exists(int i)
{
  std::ostringstream oss;
  struct stat buffer;
  oss << _basename << "/part-" <<std::setw(5)<<std::setfill('0')<< (++i);
  return (stat (oss.str().c_str(), &buffer) == 0);
}

ObjectCollection::ObjectCollection(const std::string basename)
{
  _basename = basename;
  dim = -1;
  _step = -1;
  _ffmpeg = NULL;
  _scale = -1;
  _shift[0] = 0.;
  _shift[1] = 0.;
  _rotation[0] = 0.;
  _rotation[1] = 0.;
  _clip = false;
  circleGeom.push_back(0);
  circleGeom.push_back(0);
  for(unsigned int i = 0; i <= 100; ++i) {
    float angle = i *((2.0f * 3.14159f) / 100);
    circleGeom.push_back(cos(angle));
    circleGeom.push_back(sin(angle));
  }
}

void ObjectCollection::_compute_bbox(float bbox[6]) {
  bbox[0] = bbox[2] = bbox[4] = FLT_MAX;
  bbox[1] = bbox[3] = bbox[5] = -FLT_MAX;
  std::vector<float> *vs[] = {&coord, &segcoord, &tricoord};
  for (size_t iv = 0; iv < 3; ++ iv) {
    std::vector <float> &v= *vs[iv];
    for (size_t i = 0; i < v.size(); i += dim) {
      bbox[0] = std::min(bbox[0], v[i]);
      bbox[1] = std::max(bbox[1], v[i]);
      bbox[2] = std::min(bbox[2], v[i+1]);
      bbox[3] = std::max(bbox[3], v[i+1]);
      if (dim == 3){
        bbox[4] = std::min(bbox[4], v[i+2]);
        bbox[5] = std::max(bbox[5], v[i+2]);
      }
    }
  }
}

static void abort_(const char * s, ...)
{
  va_list args;
  va_start(args, s);
  vfprintf(stderr, s, args);
  fprintf(stderr, "\n");
  va_end(args);
  abort();
}

void ObjectCollection::write_png(const char *file_name)
{
  int xywh[4];
  glGetIntegerv(GL_VIEWPORT, xywh);
  int w = xywh[2], h=xywh[3];
  FILE *fp = fopen(file_name, "wb");
  if (!fp)
    abort_("[write_png_file] File %s could not be opened for writing", file_name);
  png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!png_ptr)
    abort_("[write_png_file] png_create_write_struct failed");
  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr)
    abort_("[write_png_file] png_create_info_struct failed");
  if (setjmp(png_jmpbuf(png_ptr)))
    abort_("[write_png_file] Error during init_io");
  png_init_io(png_ptr, fp);
  if (setjmp(png_jmpbuf(png_ptr)))
    abort_("[write_png_file] Error during writing header");
  int *data = new int[w * h];
  glReadPixels(xywh[0], xywh[1], w, h, GL_RGBA, GL_UNSIGNED_BYTE, data);
  png_set_IHDR(png_ptr, info_ptr, w, h,
      8, PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE,
      PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
  png_write_info(png_ptr, info_ptr);
  if (setjmp(png_jmpbuf(png_ptr)))
    abort_("[write_png_file] Error during writing bytes");
  int **row_pointers = new int*[h];
  for (int i = 0; i < h; ++i)
    row_pointers[i] = data + w * (h - i -1); 
  png_write_image(png_ptr, (png_bytepp)row_pointers);
  if (setjmp(png_jmpbuf(png_ptr)))
    abort_("[write_png_file] Error during end of write");
  png_write_end(png_ptr, NULL);
  delete []data;
  delete []row_pointers;
  fclose(fp);
}

void ObjectCollection::start_ffmpeg(const std::string filename)
{
  if (_ffmpeg) {
    printf("ffmpeg encoding already in progress !\n");
    return;
  }
  int xywh[4];
  glGetIntegerv(GL_VIEWPORT, xywh);
  std::ostringstream oss;
  oss << "ffmpeg -r 30 -f rawvideo -pix_fmt rgba -s " << xywh[2] << "x"
      << xywh[3] << " -i - -y -vf vflip " << filename;
  _ffmpeg = popen(oss.str().c_str(), "w");
}

void ObjectCollection::stop_ffmpeg()
{
  if (_ffmpeg)
    pclose(_ffmpeg);
  _ffmpeg = NULL;
}

ObjectCollection::~ObjectCollection()
{
  stop_ffmpeg();
}
