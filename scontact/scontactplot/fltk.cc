#include "ObjectCollection.h"
#include <sstream>
#include <iomanip>

#include <FL/gl.h>
#include <FL/Fl_Toggle_Button.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Value_Slider.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Sys_Menu_Bar.H>
#include <FL/Fl_Native_File_Chooser.H>
#include <FL/Fl.H>
#include <FL/Fl_Gl_Window.H>

class ScontactPlotWindow : public Fl_Gl_Window {
  int _lastx[2];
  double _arx, _ary;
  ObjectCollection &_collection;
  public:
    void draw() {
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      glViewport(0., 0., w(), h());
      if (h() < w()) {
        _arx = (double)w() / (double)h();
        _ary = 1;
      }
      else {
        _arx = 1;
        _ary = (double)h() / (double)w();
      }

      glOrtho(-_arx, _arx, -_ary, _ary, -100, 100);
      _collection.display();
      char s[40];
      sprintf(s, "%i", _collection.step());
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity(); 
      glRasterPos2f(-0.95, -0.95);
      glColor3f(0., 0., 0.);
      gl_font(1, 12);
      gl_draw(s, strlen(s));
    }
    int handle(int event) {
      if (event == FL_FOCUS || event == FL_UNFOCUS)
        return 1;
      if (event ==FL_PUSH){
        _lastx[0] = Fl::event_x();
        _lastx[1] = Fl::event_y();
        return 1;
      }
      if (event == FL_MOUSEWHEEL) {
        double fx = (Fl::event_x()/(double)(w()) - 0.5) * 2 * _arx;
        double fy = (Fl::event_y()/(double)(h()) - 0.5) * 2 * _ary;
        if (Fl::event_dy() > 0) _collection.zoom_at(1./1.05, fx, fy);
        if (Fl::event_dy() < 0) _collection.zoom_at(1.05, fx, fy);
        redraw();
        return 1;
      }
      if (event == FL_DRAG) {
        _collection.shift(
          (Fl::event_x() - _lastx[0])/(float)(w()) * 2 * _arx,
          -(Fl::event_y() - _lastx[1])/(float)(h()) * 2 * _ary);
        _lastx[0] = Fl::event_x();
        _lastx[1] = Fl::event_y();
        redraw();
        return 1;
      }
      if (event == FL_KEYDOWN) {
        bool act = false;
        if (Fl::event_key(FL_Right)) {act = true; _collection.rotate(1, 0);}
        if (Fl::event_key(FL_Left)) {act = true; _collection.rotate(-1, 0);}
        if (Fl::event_key(FL_Up)) {act = true; _collection.rotate(0, -1);}
        if (Fl::event_key(FL_Down)) {act = true; _collection.rotate(0, 1);}
        if (act) {
          redraw();
          return 1;
        }
      }
      return Fl_Gl_Window::handle(event);
    }
    ScontactPlotWindow(ObjectCollection &collection, int X, int Y, int W, int H, const char *L=0) :
      Fl_Gl_Window(X, Y, W, H, L),
      _collection(collection)
    {
      mode(FL_ALPHA|FL_DOUBLE|FL_DEPTH|FL_MULTISAMPLE);
      end();
    };
    ObjectCollection &collection() {return _collection;}
};


static void visibility_cb(Fl_Widget *wid, void *arg) {
  ScontactPlotWindow *wingl = (ScontactPlotWindow*)arg;
  int idx = atoi(((Fl_Menu_ *) wid)->mvalue()->label() + 1);
  wingl->collection().visible_flag()[idx] ^= 2;
  wingl->collection().read();
  wingl->redraw();
}

static void visibility_bnd_cb(Fl_Widget *wid, void *arg) {
  ScontactPlotWindow *wingl = (ScontactPlotWindow*)arg;
  int idx = atoi(((Fl_Menu_ *) wid)->mvalue()->label() + 1);
  wingl->collection().visible_flag()[idx] ^= 1;
  wingl->collection().read();
  wingl->redraw();
}

static void exit_cb(Fl_Widget *wid, void *arg) {
  delete (Fl_Window*)arg;
}

static void write_png_cb(Fl_Widget *wid, void *arg) {
  ScontactPlotWindow *wingl = (ScontactPlotWindow*)arg;
  Fl_Native_File_Chooser fnfc;
  fnfc.title("Save PNG file");
  fnfc.type(Fl_Native_File_Chooser::BROWSE_SAVE_FILE);
  fnfc.filter("PNG File\t*.png");
  fnfc.preset_file("scontact.png");
  fnfc.directory(".");
  fnfc.options(Fl_Native_File_Chooser::SAVEAS_CONFIRM|Fl_Native_File_Chooser::NEW_FOLDER|Fl_Native_File_Chooser::PREVIEW);
  if (fnfc.show() == 0) 
    wingl->collection().write_png(fnfc.filename());
}

static void write_mp4_cb(Fl_Widget *wid, void *arg) {
  ScontactPlotWindow *wingl = (ScontactPlotWindow*)arg;
  Fl_Menu_Item *item = (Fl_Menu_Item*)((Fl_Menu_ *) wid)->mvalue();
  if (std::string("Start recording &MP4") == item->label()) {
    Fl_Native_File_Chooser fnfc;
    fnfc.title("Save MP4 file");
    fnfc.type(Fl_Native_File_Chooser::BROWSE_SAVE_FILE);
    fnfc.filter("MP4 File\t*.mp4");
    fnfc.preset_file("scontact.mp4");
    fnfc.directory(".");
    fnfc.options(Fl_Native_File_Chooser::SAVEAS_CONFIRM|Fl_Native_File_Chooser::NEW_FOLDER|Fl_Native_File_Chooser::PREVIEW);
    if (fnfc.show() == 0) {
      Fl_Window *mainwin = wingl->top_window();
      mainwin->size_range(mainwin->w(), mainwin->h(), mainwin->w(), mainwin->h());
      wingl->collection().start_ffmpeg(fnfc.filename());
      item->label("Stop recording &MP4");
      item->labelcolor(FL_RED);
    }
  }
  else {
    wingl->collection().stop_ffmpeg();
    wingl->top_window()->size_range(400, 400, 0, 0);
    item->label("Start recording &MP4");
    item->labelcolor(FL_BLACK);
  }
}

static void save_opt_cb(Fl_Widget *wid, void *arg) {
  ScontactPlotWindow *wingl = (ScontactPlotWindow*)arg;
  Fl_Native_File_Chooser fnfc;
  fnfc.title("Save Options");
  fnfc.type(Fl_Native_File_Chooser::BROWSE_SAVE_FILE);
  fnfc.filter("Options File\t*.opt");
  fnfc.preset_file("scontact.opt");
  fnfc.directory(".");
  fnfc.options(Fl_Native_File_Chooser::SAVEAS_CONFIRM|Fl_Native_File_Chooser::NEW_FOLDER);
  if (fnfc.show() == 0) 
    wingl->collection().save_opt(fnfc.filename());
}

static void update_menus(Fl_Menu_ *menubar, ScontactPlotWindow *wingl) {
  ObjectCollection &collection = wingl->collection();
  int idx = menubar->find_index("&Display/&Visibility");
  menubar->clear_submenu(idx); for (std::map<int, int>::iterator it = collection.visible_flag().begin(); it!= collection.visible_flag().end(); ++it) {
    std::ostringstream oss;
    oss << "&" << it->first << " " << wingl->collection().getTagName(it->first);
    menubar->insert(++idx, oss.str().c_str(), 0, visibility_cb, wingl, FL_MENU_TOGGLE | ((!(it->second &2)) ? FL_MENU_VALUE : 0));
  }
  idx = menubar->find_index("&Display/Visibility &Boundary");
  menubar->clear_submenu(idx);
  for (std::map<int, int>::iterator it = collection.visible_flag().begin(); it!= collection.visible_flag().end(); ++it) {
    std::ostringstream oss;
    oss << "&" << it->first << " " << wingl->collection().getTagName(it->first);
    menubar->insert(++idx, oss.str().c_str(), 0, visibility_bnd_cb, wingl, FL_MENU_TOGGLE | ((!it->second &1) ? FL_MENU_VALUE : 0));
  }
  Fl_Menu_Item *item = (Fl_Menu_Item*) menubar->find_item("&Display/&Clip");
  if (collection.clip())
    item->flags |= FL_MENU_VALUE;
  else
    item->flags &= ~FL_MENU_VALUE;
}

static void load_opt_cb(Fl_Widget *wid, void *arg) {
  ScontactPlotWindow *wingl = (ScontactPlotWindow*)arg;
  Fl_Native_File_Chooser fnfc;
  fnfc.title("Load Options");
  fnfc.type(Fl_Native_File_Chooser::BROWSE_FILE);
  fnfc.filter("Options File\t*.opt");
  fnfc.directory(".");
  if (fnfc.show() == 0) {
    wingl->collection().load_opt(fnfc.filename());
    update_menus((Fl_Menu_*)wid, wingl);
    wingl->collection().read();
    wingl->redraw();
  }
}

static void clip_cb(Fl_Widget *widget, void *arg) {
  ScontactPlotWindow *wingl = (ScontactPlotWindow*)arg;
  wingl->collection().clip(!wingl->collection().clip());
  wingl->collection().read();
  wingl->redraw();
}


static void Timer_CB(void *data) {
  Fl_Valuator &vals = *(Fl_Valuator*) data;
  ScontactPlotWindow *wingl = (ScontactPlotWindow*) vals.argument();
  int istep = (int)vals.value();
  int step = (int)vals.step();
  if (wingl->collection().file_exists(istep + step) && istep == wingl->collection().step()) {
    vals.value(istep + step);
    vals.do_callback();
  }
  Fl::repeat_timeout(1./12, Timer_CB, data);
}

static void play_btn_cb(Fl_Widget *widget, void *vals) {
  Fl_Button &btn = *(Fl_Button*)widget;
  btn.label(btn.value() ? "@DnArrow" : "@UpArrow");
  Fl::remove_timeout(Timer_CB, vals);
  if (btn.value()) {
    Fl::add_timeout(1./12, Timer_CB, vals);
  }
}

static void step_widget_cb(Fl_Widget *widget, void *data) {
  Fl_Valuator *val = (Fl_Valuator*)widget;
  Fl_Valuator *step = (Fl_Valuator*)data;
  step->step(val->value());
}

static void istep_counter_cb(Fl_Widget *widget, void *sw) {
  int istep = (int)((Fl_Valuator*)widget)->value();
  ScontactPlotWindow *wingl = (ScontactPlotWindow*)sw;
  if (istep != wingl->collection().step() && wingl->collection().read(istep))
    wingl->redraw();
}

static void timer_update_bound_cb(void *data) {
  Fl_Valuator &vals = *(Fl_Valuator*)data;
  ScontactPlotWindow *wingl = (ScontactPlotWindow*) vals.argument();
  int i = vals.maximum();
  if (!wingl->collection().file_exists(i))
    i = 1;
  for (; wingl->collection().file_exists(i+1); i++);
  if (i != (int)vals.maximum()) {
    vals.maximum(i);
    vals.redraw();
  }
  Fl::repeat_timeout(1, timer_update_bound_cb, data);
}


int main(int argc, char **argv) {
  ObjectCollection collection(argv[1]);
  collection.read(1);
  Fl_Window *mainwin = new Fl_Window(1000, 1000, argv[1]);
  int BH = 25, BW = 20;
  double S = 0;
  ScontactPlotWindow *wingl = new ScontactPlotWindow(collection, 0, BH, mainwin->w(), mainwin->h()-BH);
  wingl->end();
  Fl_Button *btn;
  Fl_Valuator *val;

  S += 1.5;
  Fl_Valuator *step_widget = new Fl_Value_Input(mainwin->w() - S * BW, 0, 1.5 * BW, BH);
  step_widget->minimum(1);
  step_widget->value(1);
  step_widget->maximum(100);
  step_widget->step(1);
  step_widget->box(FL_FLAT_BOX);
  step_widget->color(FL_BACKGROUND_COLOR);
  step_widget->label("+");

  S+=2;
  Fl_Toggle_Button *playbtn = new Fl_Toggle_Button(mainwin->w() - S * BW, 0, BW, BH, "@UpArrow");
  playbtn->box(FL_NO_BOX);
  playbtn->shortcut(' ');

  S += 4;
  Fl_Valuator *vals = new Fl_Slider(mainwin->w() - S * BW + 2, 2, 4 * BW - 2, BH - 4);
  vals->minimum(1);
  vals->maximum(1);
  vals->step(1);
  vals->type(FL_HOR_SLIDER);
  vals->box(FL_THIN_DOWN_BOX);
  vals->callback(istep_counter_cb, wingl);
  step_widget->callback(step_widget_cb, vals);
  playbtn->callback(play_btn_cb, vals);
  vals->value(1);
  Fl::add_timeout(0., timer_update_bound_cb, (void*)vals);
  S++;

  Fl_Menu_ *menubar = new Fl_Sys_Menu_Bar(0, 0, mainwin->w() - S * BW, BH, "File");
  Fl_Menu_Item popup[] = {
    {"&File", 0, 0, 0, FL_SUBMENU},
    {"Save as &PNG", FL_CTRL + 'p', write_png_cb, wingl},
    {"Start recording &MP4", FL_CTRL + 'm', write_mp4_cb, wingl, FL_MENU_DIVIDER},
    {"&Save Options", FL_CTRL + 's', save_opt_cb, wingl, 0},
    {"&Load Options", FL_CTRL + 'l', load_opt_cb, wingl, FL_MENU_DIVIDER},
    {"&Quit", FL_CTRL + 'q', exit_cb, mainwin},
    {0},
    {"&Display", 0, 0, 0, FL_SUBMENU},
    {"&Visibility", 0, 0, 0, FL_SUBMENU},{0},
    {"Visibility &Boundary", 0, 0, 0, FL_SUBMENU},{0},
    {"&Clip", 0, clip_cb, wingl, FL_MENU_TOGGLE},
    {0},
    {0}
  };

  menubar->global();
  menubar->menu(popup);
  update_menus(menubar, wingl);
  Fl_Group *resize_group = new Fl_Group(0, BH, mainwin->w() - S * BW, mainwin->h() - BH);
  resize_group->hide();
  Fl::focus(wingl);
  mainwin->resizable(resize_group);
  mainwin->size_range(400, 400, 0, 0);
  mainwin->end();
  mainwin->show();
  Fl::run();
  return 0;
}
