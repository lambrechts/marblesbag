#ifndef QUADTREE_H
#define QUADTREE_H
typedef struct Cell_ Cell;
Cell *cellNew(const double *xmin, const double *xmax);
void cellFree(Cell *c);
void cellAdd(Cell *c, const double *xmin, const double *xmax, int id, int **result);
void cellSearch(const Cell *c, const double *xmin, const double *xmax, int **result);
#endif
