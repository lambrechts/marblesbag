\section{Finite Element Formulation}

We present a finite element formulation for the simulation of a viscous incompressible flow that is an intermediate between a free flow (Stokes flow) and porous media flow (Darcy flow).

Consider our problem in a domain $\Omega$ with homogeneous Dirichlet boundary conditions
on its boundary $\Gamma$:
\begin{eqnarray}
\nabla p + {\mu \over K} \mvu - \bar{\mu} \nabla^2 \mvu &=& {\mathbf 0}~~\mbox{in}~~ \Omega\nonumber \\
-\nabla \cdot (\mvu + \mvu_s) &=& 0~~\mbox{in}~~ \Omega\nonumber \\
\mvu + \mvu_s& =& {\mathbf 0} ~~\mbox{on}~~ \Gamma\nonumber
\end{eqnarray}
We use the following notations:
The variational formulation of the problem is the following. Find $(\mvu,p) \in [H^1_0(\Omega)]^3 \times L^2_0(\Omega)$ such that
\begin{eqnarray}
\label{eq:var}
&&\left(\nabla p, \mvv\right)_{\Omega} + 
\left(\mvu, \nabla q\right) _{\Omega} +
\left(\bar{\mu} \nabla \mvu, \nabla \mvv \right) _{\Omega} +
\left({\mu \over K} \mvu, \mvv \right) _{\Omega} 
= \left(\nabla \cdot \mvu_s, q\right) _{\Omega}\nonumber \\
&&\forall (\mvv,q) \in [H^1_0(\Omega)]^3 \times L^2_0(\Omega).
\end{eqnarray}
Equation \eqref{eq:var} can be written in the usual abstract form: find 
$(\mvu,p) \in [H^1_0(\Omega)]^3 \times L^2_0(\Omega)$ such that


$$B(\mvu,p;\mvv,q) = L(\mvv,q),~~~~\forall (\mvv,q) \in [H^1_0(\Omega)]^3 \times L^2_0(\Omega).$$
This continuous problem has a unique solution.

We assume a partitioning ${\mathcal T}$ of the domain $\Omega$. We denote $T$ an element of the partitioning, 
and its size of $h_T$. Let us first define standard continuous finite element spaces
$${\mathbf V}_h =\left\{\mvv \in [H^1_0(\Omega) \cap C(\Omega)]^3 ~|~\mvv|_T \in [P_k(K)]^3,~~\forall T \in {\mathcal T} \right\},$$
$${ Q}_h =\left\{p \in L^2(\Omega) \cap C(\Omega)~|~p|_T \in P_k(K),~~\forall T \in {\mathcal T} \right\}.$$
We use a stabilized method for ensuring the stability of the discrete formulation: find $(\mvu_h, p_h) \in {\mathbf V}_h \times Q_h$ 
such that 
$$B_h(\mvu_h,p_h;\mvv,q)=L_h(\mvv,q)~~ \forall (\mvv_h, q_h) \in {\mathbf V}_h \times Q_h.$$
For that, we define a the \emph{stabilized bilinear form} \cite {Juntunen}
\begin{eqnarray}
&&B_h(\mvu_h,p_h;\mvv,q) = B(\mvu_h,p_h;\mvv,q) - \nonumber \\
&&\alpha \sum_{T \in {\mathcal T}} {h_T^2 \over K+h_T^2} 
\left (
\nabla p_h + {\mu \over K} \mvu_h - \mu \nabla^2 \mvu_h, 
{K\over\mu} \nabla q + \mvv - K \nabla^2 \mvv 
 \right)_T
\end{eqnarray}
and
\begin{eqnarray}
L_h(\mvv,q) = L(\mvv,q) -
\alpha \sum_{T \in {\mathcal T}} {h_T^2 \over K+h_T^2} 
\left (\nabla \cdot \mvu_s,q
\right)_T
\end{eqnarray}

Coefficient $\alpha$ has to be positive.
If linear finite elements are used, this simplifies to 
\begin{eqnarray}
&&B_h(\mvu_h,p_h;\mvv,q) = B(\mvu_h,p_h;\mvv,q) - \nonumber \\
&&\alpha \sum_{T \in {\mathcal T}} {h_T^2 \over K+h_T^2} 
\left[
\left ({K\over\mu} \nabla p_h,\nabla q\right)_T + 
\left ({\mu\over K} \mvu_h,\mvv\right)_T + 
\left (\nabla p_h,\mvv\right)_T + 
\left (\mvu_h, \nabla q\right)_T
\right].\nonumber
\end{eqnarray}
\subsection {FE formulation ($v^β = 0$)}
\begin{eqnarray*}
«φv_\alpha ·ṉ» - ‹\nabla φ·v_\alpha › &=& 0 \\
«\mu ψn · \nabla v_\alpha » -‹\mu \big(\nabla ψ·\nabla v_\alpha + ψ\nabla v^\alpha ·\nabla c_\alpha \big)› - ‹ψc_\alpha \nabla p^\alpha › -‹\frac{\mu (1-c_\alpha )²}{kc²_\alpha}v_\alpha › &=& 0
\end{eqnarray*}
where
\[
\nabla v^\alpha = \frac{\nabla v_\alpha}{c_\alpha} - \frac{v_\alpha \nabla c_\alpha}{c²_\alpha}
\]
