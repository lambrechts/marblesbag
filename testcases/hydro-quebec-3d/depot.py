#!/usr/bin/env python
import scontact3
import numpy as np
from pyfefp import mesh
import random
import os, time, sys
import shutil, random
outputdir ="output"
if not os.path.isdir(outputdir) :
    os.makedirs(outputdir)

p = scontact3.ParticleProblem()
def genInitialPosition(p, N, rmin, rmax, L, H, rhop) :
    n = 0
    RM = rmax
    R = rmin + (rmax - rmin ) * np.random.random([N])
    for z in np.arange(H-RM, -H+RM, 2* -RM) :
        for y in np.arange(L, -L, 2 * -RM) :
            for x in np.arange(L, -L, 2 * -RM) :
                R2 = x**2 + y**2
                if R2 < (L - RM*1.5)**2:
                    p.addParticle((x, y, z), R[n], 4./3*R[n]**3 * np.pi * rhop);
                    n +=1
                if n == N:
                    return

def loadMeshBoundary(p, filename, tags, shift = [0, 0, 0]):
    m = mesh.Mesh(filename)
    pnum = [m.getPhysicalNumber(2, i) for i in tags]
    addv = set()
    adde = set()
    def vshift(v) :
        return v[0] + shift[0], v[1] + shift[1], v[2] + shift[2]
    for ent in m.entities :
        if ent.dimension != 2 or not [i for i in pnum if i in ent.physicals]:
            continue
        tag = 0 if not ent.physicals else ent.physicals[0]
        stag = m.getPhysicalName(ent.dimension, tag) or str(tag)
        for i, el in enumerate(ent.elements) :
            for v in el.vertices :
                if v[3] in addv :
                    continue
                addv.add(v[3])
                p.addBoundaryDisk(vshift(v), 0.000, stag)
            for i in range(3) :
                v0 = el.vertices[i]
                v1 = el.vertices[(i + 1) % 3]
                if set((v0[3], v1[3])) in adde :
                    continue
                p.addBoundarySegment(vshift(v0), vshift(v1), stag)
            p.addBoundaryTriangle(vshift(el.vertices[0]), vshift(el.vertices[1]), vshift(el.vertices[2]), stag)

r = 0.02 * 2
dr = 0.002 *2
N = 8000
genInitialPosition(p, N = N, rmin = r -dr, rmax = r + dr , L = 1, H = 1.5, rhop=1200 * 2./3)
p.useJacobi(False)
loadMeshBoundary(p, "mesh.msh", ["Box"])

g = 1
tEnd = 10
t = 0
iter = 0
tic = time.clock()
p.write("%s/part-%05i" % (outputdir, 0))
ioutput = 0
p.velocity()[:, :] = 0;
while t < tEnd :
    p.externalForces()[:, :] = - p.velocity()
    p.externalForces()[:, 2] = -g
    p.externalForces()[:, :] *= p.particles()[:, [1]]
    dt = p.maxdt(r +dr)/5
    print(dt)
    p.iterate(r + dr, dt, 1e-4)
    t += dt
    iter += 1
    if iter %10 == 0 :
        outputname = "%s/part-%05i" % (outputdir, ioutput)
        tmpoutputname = "%s/part-%05i_tmp" % (outputdir, ioutput)
        ioutput += 1
        p.write(tmpoutputname)
        shutil.move(tmpoutputname, outputname)
print("cpu time : %g" % (time.clock() - tic))
loadMeshBoundary(p, "mesh.msh", ["Pile"], [0, 0, 0.5])
p.write("deposited")
