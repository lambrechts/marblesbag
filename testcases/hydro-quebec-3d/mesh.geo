L = 1;
l = 0.2;
H = 1.5;
Lc = 0.3;
h = 0.5;
lc = 0.2;

Point(1) = {-L, 0, H, Lc};
Point(2) = {0, -L, H, Lc};
Point(3) = {L, 0, H, Lc};
Point(4) = {0, L, H, Lc};
Point(5) = {0, 0, H, Lc};
Circle(1) = {1, 5, 2};
Circle(2) = {2, 5, 3};
Circle(3) = {3, 5, 4};
Circle(4) = {4, 5, 1};
Line Loop(1) = {1, 2, 3, 4};
Plane Surface(1) = {1};

Point(6) = {-l, 0, h, lc};
Point(7) = {0, -l, h, lc};
Point(8) = {l, 0, h, lc};
Point(9) = {0, l, h, lc};
Point(10) = {0, 0, h, lc};
Circle(6) = {6, 10, 7};
Circle(7) = {7, 10, 8};
Circle(8) = {8, 10, 9};
Circle(9) = {9, 10, 6};
Line Loop(2) = {6, 7, 8, 9};
Plane Surface(2) = {2};

tmp[] = Extrude {0, 0, -2*H}{
  Surface{1};
};

tmp2[] = Extrude {0, 0, -2*h}{
  Surface{2};
};

Delete {Volume {tmp[1],tmp2[1]};}
Surface Loop(10) = {1, tmp[0], tmp[2], tmp[3], tmp[4], tmp[5]};
Surface Loop(11) = {2, tmp2[0], tmp2[2], tmp2[3], tmp2[4], tmp2[5]};
Volume(10) = {10, 11};
Physical Volume("Domain") = {10};
Physical Surface("Box") = {tmp[0], tmp[2], tmp[3], tmp[4], tmp[5]};
Physical Surface("PtFix") = {1};
Physical Surface("Pile") = {2, tmp2[0], tmp2[2], tmp2[3], tmp2[4], tmp2[5]};
//Physical Point("PtFix") = {1};
