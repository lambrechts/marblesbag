from .meshJacobian import MeshJacobian
from .particleFluid import ParticleFluid
from .Brinkman import Brinkman
from .dofManager import DofManager
from .Steady import Steady
from .mesh import Mesh
from .legendre import *
import time

class FluidSolver :
    def __init__(self, mesh, dt, rhop, rho, mu, g, imex=True) :
        if isinstance(mesh, str):
            mesh = Mesh(mesh)
        self._mesh = mesh
        self._jac = MeshJacobian(mesh)
        self._pf = ParticleFluid(self._jac)
        self._imex = imex
        self._dt = dt
        self._stabFactor = 1
        law = Brinkman(
            mu = mu,
            pf = self._pf,
            ns = False,
            g = np.array(g),
            rho = rho,
            rhop = rhop,
            D = self._jac.dim,
            stab = dt)
        d = DofManager(mesh)
        if self._jac.dim == 2 :
            d.add_field(TriangleP1Mini)
            d.add_field(TriangleP1Mini)
            d.add_field(TriangleP1)
        elif self._jac.dim == 3:
            d.add_field(TetrahedronP1Mini)
            d.add_field(TetrahedronP1Mini)
            d.add_field(TetrahedronP1Mini)
            d.add_field(TetrahedronP1)
        else :
            raise ValueError("invalid mesh dimension : %i" % self._jac.dim)
        self._dt = dt
        self._law = law
        self._dof = d
        self._sol = None

    def add_boundary_condition(self, tag, field, *a, **b) :
        self._dof.add_boundary_condition(tag, field, *a, **b)

    def complete(self) :
        self._dof.complete()
        self._sol = self._dof.new_vector()
        self._solver = Steady(self._law, self._dof, self._jac)
        self._solverEx = Steady(self._law, self._dof, self._jac)

    def solve(self) :
        tic = time.clock()
        if self._imex :
            v0 = self._pf.velocity.copy()
            self._law.stab = self._dt *0.5 * self._stabFactor
            self._sol[:] = self._solver.solve()
            self._pf.velocity += self._pf.forces * self._dt*0.5 / self._pf.mass
            self._law.stab = 0
            self._sol[:] = self._solverEx.solve()
            self._pf.velocity[:] = v0
        else :
            self._law.stab = self._dt * self._stabFactor
            self._sol[:] = self._solver.solve()

        print("cpu : ", time.clock() - tic)
        return self.forces

    def set_stab_factor(self, s) :
        self._stabFactor = s

    def write_position(self, odir, oiter, time, filetype="msh"):
        if filetype == "msh" :
            basename = "%s/f_%05i" % (odir, oiter)
            if self._jac.dim == 2 :
                X = np.zeros(((self._jac.dim + 1) *3, self._jac.x.shape[0]))
                X[[0,1,3,4,6,7], :] = self._jac.x.reshape([-1, 6]).T
            else :
                X = self._jac.x.reshape(-1, 12).T
            self._jac.writeScalar(basename + "-xmesh.msh", "x", oiter, time, X)
        else :
            print("unknown export format : \"%s\"." % filetype)

    def write_solution(self, odir, oiter, time, filetype="msh") :
        dim = self._jac.dim
        if filetype == "msh" :
            basename = "%s/f_%05i" % (odir, oiter)
            self._jac.writeScalar(basename + "-porosity.msh", "porosity", oiter, time, self._jac.dofManager.getField(0, self._pf.porosity))
            self._jac.writeScalar(basename + "-pressure.msh", "pressure", oiter, time, self._dof.getField(dim, self._sol))
            X = np.zeros(((dim+1)*3, self._jac.x.shape[0]))
            for d in range (dim) :
                X[d::3, :] = self._dof.getField(d, self._sol)[:-1,:]
            self._jac.writeScalar(basename + "-velocity.msh", "velocity", oiter, time, X)
        elif filetype == "vtk" :
            output = open("%s/f_%05i.vtu" %(odir, oiter), "w")
            vertices = np.array(self._mesh.vertices)[:,:3]
            elements = np.array([[v[3] for v in e.vertices] for e in self._dof.getElements(0)], np.int32) -1
            v = np.zeros((vertices.shape[0], 3))
            for d in range (dim) :
                v[elements, d] = self._jac.x[:,:,d]
            output.write("<VTKFile type=\"UnstructuredGrid\" format=\"0.1\">\n")
            output.write("<UnstructuredGrid>\n")
            output.write("<Piece NumberOfPoints=\"%i\" NumberOfCells=\"%i\">\n" % (vertices.shape[0], elements.shape[0]))
            output.write("<Points>\n")
            output.write("<DataArray NumberOfComponents=\"3\" type=\"Float64\" format=\"ascii\">\n")
            np.savetxt(output, v)
            output.write("</DataArray>\n")
            output.write("</Points>\n")
            output.write("<Cells>\n")
            output.write("<DataArray  type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n")
            np.savetxt(output, elements, fmt="%i")
            output.write("</DataArray>\n")
            output.write("<DataArray  type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n")
            np.savetxt(output, (np.array(range(elements.shape[0]), np.int32)+1)*(dim+1), fmt="%i", newline=" ")
            output.write("</DataArray>\n")
            output.write("<DataArray  type=\"Int32\" Name=\"types\" format=\"ascii\">\n")
            t = np.ndarray((elements.shape[0]), np.int32)
            t[:] = 5 if dim == 2 else 10
            np.savetxt(output, t, fmt="%i", newline=" ")
            output.write("</DataArray>\n")
            output.write("</Cells>\n")
            output.write("<PointData Scalars=\"Pressure Porosity\" Vectors=\"Velocity\">\n")
            output.write("<DataArray Name=\"Porosity\" NumberOfComponents = \"1\" type=\"Float64\" format=\"ascii\">")
            np.savetxt(output, self._pf.porosity)
            output.write("</DataArray>\n")
            output.write("<DataArray Name=\"Pressure\" NumberOfComponents = \"1\" type=\"Float64\" format=\"ascii\">")
            p = np.ndarray((vertices.shape[0]))
            p[np.transpose(elements)] = self._dof.getField(dim, self._sol)
            np.savetxt(output, p)
            output.write("</DataArray>\n")
            output.write("<DataArray Name=\"Velocity\" NumberOfComponents = \"3\" type=\"Float64\" format=\"ascii\">")
            v = np.zeros((vertices.shape[0], 3))
            for d in range (self._jac.dim) :
                v[np.transpose(elements), d] = self._dof.getField(d, self._sol)[:self._jac.dim+1,:]
            np.savetxt(output, v)
            output.write("</DataArray>\n")
            output.write("</PointData>\n")
            output.write("</Piece>\n")
            output.write("</UnstructuredGrid>\n")
            output.write("</VTKFile>\n")
        else :
            print("unknown export format : \"%s\" (choose \"vtk\" or \"msh\")." % filetype)

    def mesh_position(self) :
        return self._jac.x

    def set_mesh_position(self, x) :
        self._jac.update(x)

    def set_particles(self, massp, volume, position, velocity) :
        self.forces = np.zeros_like(velocity)
        self._pf.set_particles(position, volume, velocity, self.forces, massp)
