import pyfefp
import numpy as np
import time
import os

mu = 100e-3 #100e-3#588e-3
outputdir = "output/mu%.10g" % mu
if not os.path.isdir(outputdir) :
    os.makedirs(outputdir)

## scontact
from pyfefp import scontact2Interface
p = scontact2Interface.ScontactInterface("deposited")

tEnd = 200
dt = 0.02
g =  [0, 0]
r = np.min(p.r())
print("th dt : %g dt %g" % (r / 0.0064, dt))
rhop = np.mean(p.mass()/p.volume())

rho = 1.253e3
print(rho, rhop)

outf = 1
fluid = pyfefp.FluidSolver("disk.msh", dt, rhop=rhop, rho=rho, g = g, mu=mu, imex=False)
fluid.add_boundary_condition("Inner", 0, cb = lambda x : -x[:, 1])
fluid.add_boundary_condition("Inner", 1, cb = lambda x : x[:, 0])
fluid.add_boundary_condition("Outer", 0, 0.)
fluid.add_boundary_condition("Outer", 1, 0.)
fluid.add_boundary_condition("PtFix", 2, 0.)
#this has NO justification, this testcase does not really work !!
fluid.set_stab_factor(100)
fluid.complete()

t = 0
ii = 0
outputname = "%s/part-%05i" % (outputdir, 0)
p.write(outputdir, 0, 0.)

fluid.set_particles(p.mass(), p.volume(), p.position(), p.velocity())
fluid.write_solution(outputdir, 0, 0.)

tic = time.clock()
while t < tEnd :
    p._p.externalForces()[:] = fluid.solve()
    p.iterate(dt, p._p.externalForces())
    fluid.set_particles(p.mass(), p.volume(), p.position(), p.velocity())

    t += dt
    if ii %outf == 0 :
        ioutput = int(ii/outf) + 1
        p.write(outputdir, ioutput, t)
        fluid.write_solution(outputdir, ioutput, t)
    ii += 1
    print("%i : %.2g/%.2g (cpu %.2g)" % (ii, t, tEnd, time.clock() - tic))
