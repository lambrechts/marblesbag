import numpy as np
from . import quadrature

_u_ = 0
_v_ = 1
_w_ = 2

def derivate(f, x, nd = 1, eps = 1e-8) :
    f0 = f(x)
    xd = x.reshape(x.shape[:x.ndim-nd]+(-1,))
    d = np.ndarray((f0.shape +(xd.shape[-1],)))
    for i in range(xd.shape[-1]) :
        xd[..., i] += eps
        d[..., i] = (f(xd.reshape(x.shape)) - f0) / eps
        xd[..., i] -= eps
    return d.reshape(f0.shape + x.shape[x.ndim - nd:])


class Brinkman :

    def dot(self, a, b) :
        return a[..., 0] * b[..., 0] + a[..., 1] * b[..., 1] + (0 if self.D == 2 else a[..., 2] * b[..., 2])

    def psiTerm0(self, sol, solgrad):
        _p_ = self.D
        _U_ = range(self.D)
        val = np.zeros_like(sol)
        #self.g[None, None, :] * self.ca * self.rhof
        val[..., _U_] =  \
            - solgrad[..., _p_, :] \
            + self.mu * (self.dot(solgrad[...,_U_,:], self.dca)-sol[...,_U_]/self.ca**2*self.dot(self.dca, self.dca))
        val[..., _p_] = - solgrad[..., _u_, 0] - solgrad[..., _v_, 1] - (0 if self.D == 2 else solgrad[..., _w_, 2])
        val[..., _p_] += - self.dvs[..., 0, 0] - self.dvs[..., 1, 1] - (0 if self.D == 2 else self.dvs[..., 2, 2])
        return val

    def psiTerm1(self, sol, solgrad):
        _p_ = self.D
        _U_ = range(self.D)

        val = np.ndarray(sol.shape + (self.D,))
        val[..., _U_, :] = -solgrad[..., _U_, :] * self.mu
        val[..., _p_, :] =  0#self.vs# + sol[..., _U_]
        if self.ns :
            val[..., _u_, :] += - sol[..., [_u_]] * sol[..., _U_] / self.ca * self.rhof
            val[..., _v_, :] += - sol[..., [_v_]] * sol[..., _U_] / self.ca * self.rhof
            if self.D == 3:
                val[..., _w_, :] += - sol[..., [_w_]] * sol[..., _U_] / self.ca * self.rhof
        return val

    # forces by the particle ON the fluid
    def particleForces(self, sol, solgrad, fonpart = None) :
        _p_ = self.D
        _U_ = range(self.D)
        val = np.zeros_like(sol)
        if self.D == 2 :
            d = 2 * (self.pf.volume[0, 0] * 3./2 / np.pi)**0.5
        else :
            d = 2 * (3./(4 * np.pi) * self.pf.volume[0, 0])**(1./3)
        U = self.vs - sol[..., _U_]/self.ca
        normU = np.sqrt(self.dot(U, U))[...,np.newaxis]
        Re_O_u = d * self.ca/self.mu * self.rhof
        Cd_u = (0.63 * normU + 4.8/(Re_O_u**0.5))**2
        f = self.ca**(-1.8)
        F = solgrad[..., _p_, :]
        GoU = f * Cd_u * self.rhof / 2
        #mass = self.pf.volume * 3/2 * self.rhop
        aext = self.g * (self.rhop - self.rhof) /self.rhop
        UImp = U + aext * self.stab
        if fonpart is not None:
            fonpart[...] = (-F - GoU*UImp) / (1 + self.stab/self.pf.mass * GoU) + aext * self.pf.mass
        val[..., _U_] =  (F + GoU* UImp) / (1 + self.stab/self.pf.mass * GoU)
        val[..., _p_] = 0

        return val

    def __init__(self, mu, pf, g, rho, rhop, ns = False, stab = 0, D = 2) :
        self.D = D
        self.ns = ns
        self.rhof = rho
        self.rhop = rhop
        self.stab = stab
        self.mu = mu
        self.pf = pf
        self.g = np.array(g)

    def particleInteractionTerm(self, meshJ, solution, res, jac) :
        _p_ = self.D
        _U_ = range(self.D)
        dofm = solution.dof_manager()
        self.ca = self.pf.porosity.at_points(self.pf.eid, self.pf.uvw)
        self.vs = self.pf.velocity
        sol = solution.at_points(self.pf.eid, self.pf.uvw)
        gradsol = solution.gradient_at_points(self.pf.eid, self.pf.uvw, meshJ)
        pTerm = self.particleForces(sol, gradsol, self.pf.forces) * self.pf.volume
        for i in range(dofm.n_fields()) :
            np.add.at(res, dofm._fields[i][0][:, self.pf.eid], (dofm.get_field_basis(i).f(self.pf.uvw)*pTerm[:, [i]]).T)
        d00 = derivate(lambda s : self.particleForces(s, gradsol), sol, 1) * self.pf.volume[..., None]
        d01 = derivate(lambda s : self.particleForces(sol, s), gradsol, 2) * self.pf.volume[..., None, None]
        for f in range(dofm.n_fields()) :
            ff = dofm.get_field_basis(f).f(self.pf.uvw)
            for g in range(dofm.n_fields()) :
                pp = d00[:, f, g, None] * dofm.get_field_basis(g).f(self.pf.uvw)
                d01xi = meshJ.multDXiDXLeft(d01[:, f, g, :], 1, self.pf.eid)
                pp += (d01xi[:, None, :] * dofm.get_field_basis(g).df(self.pf.uvw)).sum(2)
                jac.add_to_field(f, g, pp[:, None, :] * ff[:, :, None], self.pf.eid)

    def fluidTerm(self, meshJ, solution, res, jac) :
        dofm = solution.dof_manager()
        qp, qw = quadrature.get_triangle(3) if self.D == 2 else quadrature.get_tetrahedron(3)
        self.vs = self.pf.momentum.at_qp(qp)
        self.dvs = self.pf.momentum.gradient_at_qp(qp, meshJ)
        self.ca = self.pf.porosity.at_qp(qp)
        self.dca = self.pf.porosity.gradient_at_qp(qp, meshJ)
        sol = solution.at_qp(qp)
        gradsol = solution.gradient_at_qp(qp, meshJ)

        x0 = meshJ.multJ(self.psiTerm0(sol, gradsol))
        x1 = meshJ.multJ(meshJ.multDXiDXLeft(self.psiTerm1(sol, gradsol), 3))
        for i in range(dofm.n_fields()) :
            basis = dofm.get_field_basis(i)
            psiW = basis.f(qp) * qw[:, None]
            r0 = np.tensordot(psiW, x0[:, :, i], [0, 1])
            dPsiW = basis.df(qp) * qw[:, None, None]
            r1 = np.tensordot(dPsiW, x1[:, :, i, :], [[0, 2], [1, 2]])
            np.add.at(res, dofm._fields[i][0], r0 + r1)

        d00 = derivate(lambda s : self.psiTerm0(s, gradsol), sol, 1)
        d01 = derivate(lambda s : self.psiTerm0(sol, s), gradsol, 2)
        d10 = derivate(lambda s : self.psiTerm1(s, gradsol) , sol, 1)
        d11 = derivate(lambda s : self.psiTerm1(sol, s), gradsol, 2)
        for f in range(dofm.n_fields()):
            basisf = dofm.get_field_basis(f)
            for g in range(dofm.n_fields()):
                basisg = dofm.get_field_basis(g)
                if (np.max(np.abs(d00[...,f,g])) > 1e-8) :
                    psiPsiW = np.einsum("qn, qm, q-> qnm", basisf.f(qp), basisg.f(qp), qw)
                    jac.add_to_field(f, g, np.tensordot(meshJ.multJ(d00[:,:,f,g]), psiPsiW, 1))
                if (np.max(np.abs(d01[:,:,f,g,:])) > 1e-8) :
                    dPsiPsiW = np.einsum("qmd, qn, q -> qdnm", basisg.df(qp), basisf.f(qp), qw)
                    jac.add_to_field(f, g, np.tensordot(meshJ.multJ(meshJ.multDXiDXLeft(d01[:,:,f,g,:], 2)), dPsiPsiW, 2))
                if (np.max(np.abs(d10[:,:,f,:,g])) > 1e-8) :
                    psiDPsiW = np.einsum("qmd, qn, q -> qdmn", basisf.df(qp), basisg.f(qp), qw)
                    jac.add_to_field(f, g, np.tensordot(meshJ.multJ(meshJ.multDXiDXLeft(d10[:,:,f,:,g], 2)), psiDPsiW, 2))
                if (np.max(np.abs(d11[:,:,f,:,g,:])) > 1e-8) :
                    dPsiDPsiW = np.einsum("qmx, qny, q -> qxymn", basisf.df(qp), basisg.df(qp), qw)
                    jac.add_to_field(f, g, np.tensordot(meshJ.multDXiDXLeft(meshJ.multDXiDXLeft(meshJ.multJ(d11[:,:,f,:,g,:]), 3), 2), dPsiDPsiW, 3))

    def massTerm(self, alpha, solution, res, jac) :
        pass

    def volumeTerm(self, meshJ, solution) :
        dofm = solution.dof_manager()
        jac = dofm.new_matrix()
        res = dofm.new_vector()
        self.particleInteractionTerm(meshJ, solution, res, jac)
        self.fluidTerm(meshJ, solution, res, jac)
        #self.massTerm(meshJ, alphamass, solution, res, jac)
        return res, jac.to_csr_matrix()
