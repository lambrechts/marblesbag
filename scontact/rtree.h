typedef struct Rtree_ Rtree;
Rtree *rtreeNew();
void rtreeFree(Rtree*);
void rtreeInsert(Rtree*, double*amin, double *amax, int id);
void rtreeSearch(Rtree*, double *amin, double *amax, double alert, int **result, int *n);
