L = 2;
l = 0.1;
H = 1.5;
h = 0.5;
y = 0;
lc = 0.1;

Point(1) = {-L, H, 0, lc};
Point(2) = {-L, -H, 0, lc};
Point(3) = {L, -H, 0, lc};
Point(4) = {L, H, 0, lc};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(1) = {1:4};

Point(11) = {-l, y + h, 0, lc};
Point(12) = {l, y + h, 0, lc};
Point(13) = {l, y - h, 0, lc};
Point(14) = {-l, y - h, 0, lc};
Line(11) = {11, 12};
Line(12) = {12, 13};
Line(13) = {13, 14};
Line(14) = {14, 11};
Line Loop(2) = {11:14};

Plane Surface(1) = {1, 2};
Physical Line("Box") = {1,2,3};
Physical Line("Top") = {4};
Physical Line("Pile") = {11:14};
Physical Surface("Domain") = {1};
Physical Point("PtFix") = {1};
